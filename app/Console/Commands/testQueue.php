<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Mookofe\Tail\Facades\Tail;

class testQueue extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'testQueue';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        Tail::listen('movie', function ($message) {
            var_dump($message);
            return 'okie';
        });
    }
}
