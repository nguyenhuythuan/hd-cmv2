<?php

namespace App\Helpers\Constants;

/**
 * Class Auth.
 */
class JwtToken
{
    const GUEST_TOKEN = 'Guest-Token';
    const ACCESS_TOKEN = 'Access-Token';
    const REFRESH_TOKEN = 'Refresh-Token';
    const TOKEN = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzaWQiOiIxMjM0NTY3ODkwIiwidWlkIjoibmhhcHEiLCJpYXQiOiJKb2huIERvZSIsInBsdCI6IlRWOlNBTVNVTkc6QUJDIiwianRpIjoiMTIzNDUiLCJhaWQiOjE1MTYyMzkwMjJ9.5y2uovBTP63ym0dbHmvc_HdDMv-H4eRaaUzFjv1xidA';
}
