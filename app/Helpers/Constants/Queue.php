<?php

namespace App\Helpers\Constants;

/**
 * Class Auth.
 */
class Queue
{
    const EXCHANGE = 'cm.index';
    const EXCHANGE_TYPE = 'topic';
    const ROUTING_KEY_TITLE_CREATE = 'title.create.*';
    const ROUTING_KEY_TITLE_UPDATE = 'title.update.*';
    const ROUTING_KEY_TITLE_DELETE = 'title.delete.*';
    const ROUTING_KEY_PAGE_CREATE = 'page.create.*';
}
