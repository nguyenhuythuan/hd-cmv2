<?php

namespace App\Http\Controllers\Backend\Auth\Permission;

//use App\Models\Auth\Role;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\Role\ManageRoleRequest;

/**
 * Class RoleController.
 */
class PermissionController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    /**
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(PermissionRepository $permissionRepository, RoleRepository $roleRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
    }

    /**
     * @param ManageRoleRequest $request
     *
     * @return mixed
     */
    public function index(ManageRoleRequest $request)
    {
        return view('backend.auth.permission.index')
            ->withPermissions($this->permissionRepository
            ->with('users', 'permissions')
            ->orderBy('id')
            ->paginate());
    }

}
