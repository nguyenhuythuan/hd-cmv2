<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Mail\Frontend\SendMailUrl;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

/**
 * Class DashboardController.
 */
class DashboardController extends Controller
{
    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.dashboard');
    }

    public function sendMailUrl(Request $request)
    {
        $user =\Auth::user();
        $name = $user->getNameAttribute();
        $link = config('services.link.url');
        $toEmail = $user->getEmailForPasswordReset();
        $message = config('services.link.message');
        $request->name = $name;
        $request->link = $link;
        $request->toEmail = $toEmail;
        $request->message = $message;
        Mail::send(new SendMailUrl($request));
        return redirect()->back()->withFlashSuccess(__('alerts.frontend.contact.sent'));

    }
}
