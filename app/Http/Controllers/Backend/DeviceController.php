<?php

namespace App\Http\Controllers\Backend;

use App\Repositories\Backend\DeviceRepository;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DeviceController extends Controller
{
    /**
     * @var DeviceRepository
     */
    protected $deviceRepository;

    /**
     * DeviceController constructor.
     * @param DeviceRepository $deviceRepository
     */
    public function __construct(DeviceRepository $deviceRepository)
    {
        $this->deviceRepository = $deviceRepository;
    }

    /**
     * @return \Illuminate\View\View
     */
    public function index()
    {
        return view('backend.device');
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getList(Request $request)
    {
        return $this->deviceRepository->where('path', $request->path)->groupBy(['platform'])->get(['modelName', 'platform', 'path'])->toJson();
    }

    /**
     * @param Request $request
     * @return string
     */
    public function getDetail(Request $request) {
        return $this->deviceRepository->where('platform', $request->platform, '=')->get(['macAddress', 'modelName', 'platform', 'modelId', 'userId'])->toJson();
    }
}
