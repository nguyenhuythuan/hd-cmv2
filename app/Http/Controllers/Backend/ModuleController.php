<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\ModuleRepository;
use Illuminate\Http\Request;

/**
 * Class ModuleController.
 */
class ModuleController extends Controller
{
    /**
     * @var ModuleRepository
     */
    protected $moduleRepository;

    /**
     * ModuleRepository constructor.
     *
     * @param ModuleRepository $moduleRepository
     */
    public function __construct(ModuleRepository $moduleRepository)
    {
        $this->moduleRepository = $moduleRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.module.index');
//            ->withModule($this->moduleRepository->getActivePaginated(25, 'id', 'asc'));
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(Request $request)
    {
        $paramSearch = $request->search;
        $paramDirection = $request->direction;
        $paramColumn = $request->column;
        $perPage = $request->perPage;
        $loadData = $this->moduleRepository;

        if ($paramSearch) {
            $loadData->where('name_module', '%'. $paramSearch . '%', 'like');
        }

        if ($paramColumn) {
            $loadData->orderBy($paramColumn,$paramDirection);
        }

        return $loadData->paginate(
            $perPage,
            [
                'nameModule',
                'name',
                'slug',
                'type',
                'transactionTip',
                'profileType',
            ],
            'page',
            null
        );
    }

    public function create()
    {
        return view('backend.module.create');
    }
}
