<?php

namespace App\Http\Controllers\Backend;

//use App\Models\Auth\Role;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\Auth\RoleRepository;
use App\Repositories\Backend\Auth\PermissionRepository;
use App\Http\Requests\Backend\Auth\Role\ManageRoleRequest;
use App\Repositories\Backend\MongoDB\ChecksumRepository;
use App\Repositories\Backend\MongoDB\TitleRepository;
use App\Repositories\Backend\MongoDB\PeopleRepository;
use App\Repositories\Backend\MongoDB\RibbonRepository;
use App\Repositories\Backend\MongoDB\DamRepository;
use Illuminate\Http\Request;
use MongoDB\BSON\ObjectId;

use Illuminate\Support\Facades\Storage;
use Kontoulis\RabbitMQLaravel\Facades\RabbitMQ;
use App\Helpers\Constants\Queue;
use App\Helpers\Constants\JwtToken;
use App\Http\Requests\Backend\UploadImageRequest;

/**
 * Class RoleController.
 */
class MovieContentController extends Controller
{
    /**
     * @var RoleRepository
     */
    protected $roleRepository;

    /**
     * @var PermissionRepository
     */
    protected $permissionRepository;

    /**
     * @var TitleRepository
     */
    protected $titleRepository;

    /**
     * @var PeopleRepository
     */
    protected $peopleRepository;

    /**
     * @var RibbonRepository
     */
    protected $ribbonRepository;

    /**
     * @var DamRepository
     */
    protected $damRepository;

    /**
     * @var ChecksumRepository
     */
    protected $checksumRepository;

    /**
     * @param RoleRepository       $roleRepository
     * @param PermissionRepository $permissionRepository
     */
    public function __construct(
        PermissionRepository $permissionRepository,
        RoleRepository $roleRepository,
        TitleRepository $titleRepository,
        PeopleRepository $peopleRepository,
        RibbonRepository $ribbonRepository,
        ChecksumRepository $checksumRepository,
        DamRepository $damRepository
    )
    {
        $this->roleRepository = $roleRepository;
        $this->permissionRepository = $permissionRepository;
        $this->titleRepository = $titleRepository->getModel();
        $this->peopleRepository = $peopleRepository->getModel();
        $this->ribbonRepository = $ribbonRepository->getModel();
        $this->checksumRepository = $checksumRepository->getModel();
        $this->damRepository = $damRepository->getModel();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        if ($request->ajax()) {
            switch ($request->status) {
                case 'active':
                    $listTitle = $this->titleRepository::where('status', '=', 'active')->orderBy('datePublished', 'desc')->paginate(10)->toArray();
                    break;
                case 'unActive':
                    $listTitle = $this->titleRepository::where('status', '=', 'unActive')->orderBy('datePublished', 'desc')->paginate(10)->toArray();
                    break;
                case 'inReview':
                    $listTitle = $this->titleRepository::where('status', '=', 'inReview')->orderBy('datePublished', 'desc')->paginate(10)->toArray();
                    break;
                default:
                    $listTitle = $this->titleRepository::paginate(10)->toArray();
            }
            $data['docs'] = $this->formatListTitleIndex($listTitle['data']);
            $data['total'] = $listTitle['total'];

            return response()->json($data, 200);
        } else {
            $activeTitle = json_encode($this->formatListTitleIndex($this->titleRepository::where('status', '=', 'active')->orderBy('datePublished', 'desc')->take(10)->get()->toArray()));
            $unActiveTitle = json_encode($this->formatListTitleIndex($this->titleRepository::where('status', '=', 'unActive')->orderBy('datePublished', 'desc')->take(10)->get()->toArray()));
            $inReviewTitle = json_encode($this->formatListTitleIndex($this->titleRepository::where('status', '=', 'inReview')->orderBy('datePublished', 'desc')->take(10)->get()->toArray()));

            return view('backend.movie_content.index')->with(compact(['activeTitle', 'unActiveTitle', 'inReviewTitle']));
        }
    }

    protected function formatListTitleIndex($arrayTitle)
    {
        $formatTitle = [];
        foreach ($arrayTitle as $title) {
            $element = [];
            $element['_id'] = isset($title['_id']) ? $title['_id'] : null;
            $element['legacy_id'] = isset($title['legacy_id']) ? $title['legacy_id'] : null;
            $element['alternateName'] = isset($title['alternateName']) ? $title['alternateName'] : null;
            $element['datePublished'] = isset($title['datePublished']) ? $title['datePublished'] : null;
            $element['priceType'] = isset($title['priceType']) ? $title['priceType'] : null;
            $element['type'] = isset($title['type']) ? $title['type'] : null;
            $element['copyrightExpireOn'] = isset($title['copyrightExpireOn']) ? $title['copyrightExpireOn'] : null;
            $element['status'] = isset($title['status']) ? $title['status'] : null;
            $element['image']['web']['thumbnail'] = isset($title['image']['web']['thumbnail']) ? $title['image']['web']['thumbnail'] : null;
            if (isset($title['image']['web']['thumbnail'])) {
                foreach($title['image']['web']['thumbnail'] as $img){
                    $element['thumbnail'] = $img['status'] == 'active' ? $img['url'] : null;
                }
            }

            array_push($formatTitle, $element);
        }
        return $formatTitle;
    }

    public function ajaxSuggest(Request $request)
    {
        if ($request->ajax())
        {
            $keyword = $request->keyword;
            $size = $request->size;
            $grateful = $request->grateful;

            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://agw.fimplus.io/movieIdx/suggest', [
                'headers'        => [JwtToken::ACCESS_TOKEN => JwtToken::TOKEN],
                'query' => [
                    'q' => $keyword,
                    'size' => $size,
                    'grateful' => $grateful
                ],
            ]);

            $result = $response->getBody()->getContents();
            if (count((array) json_decode($result)) == 0) {
                $response = $client->request('GET', 'https://agw.fimplus.io/movieIdx/suggest', [
                    'headers'        => [JwtToken::ACCESS_TOKEN => JwtToken::TOKEN],
                    'query' => [
                        'q' => $keyword,
                        'size' => $size,
                        'grateful' => 1
                    ],
                ]);

                $result = $response->getBody()->getContents();
            };
            return $result;
        }
    }

    public function ajaxSearch(Request $request) {
        if ($request->ajax())
        {
            $keyword = $request->keyword;
            $status = $request->status;
            $size = $request->size;
            $page = $request->page;
            $client = new \GuzzleHttp\Client();
            $response = $client->request('GET', 'https://agw.fimplus.io/movieIdx/cm/search', [
                'headers'        => [JwtToken::ACCESS_TOKEN => JwtToken::TOKEN],
                'query' => [
                    'q' => $keyword,
                    'status' => $status,
                    'size' => $size,
                    'page' => $page
                ],
            ]);
            $content = json_decode($response->getBody()->getContents());
            $result = [];
            if ($content->docs != null) {
                $listTitleId = array_column($content->docs, '_id');
                $result['docs'] = $this->formatListTitleIndex($this->titleRepository::whereIn('_id', $listTitleId)->get()->toArray());
                $result['total'] = $content->total;
            }

            return $result;
        }
    }

    /**
     * @param $id
     */
    public function edit($id)
    {
        $title = $this->titleRepository::where('legacy_id', '=', $id)->first();
        
        // Get detail actor
        $actor = isset($title->actor) ? json_encode($this->getListActorDetail($title->actor)) : json_encode([]);
        
        // Get detail director
        $director = isset($title->director) ? json_encode($this->getListDirectorDetail($title->director)): json_encode([]);

        // Get list ribbon
        $ribbon = isset($title->_id) ? json_encode($this->getListRibbon($title->_id)) : [];

        // Get list image
        $photo = isset($title->image) ? json_encode($this->getListImage($title->image)) : json_encode([]);

        // Get list related
        $related = isset($title->related) ? json_encode($this->getListRelatedDetail($title->related)) : json_encode([]);

        $damInfo = isset($title->bindTo) ? json_encode($this->getDamInfo($title->bindTo)) : json_encode((object)[]);

        return view('backend.movie_content.edit')->with(compact(['title', 'actor', 'director', 'ribbon', 'photo', 'related', 'damInfo']));
    }

    protected function getListActorDetail($actorIdArray)
    {
        $listActorDetail = $this->peopleRepository::whereIn('_id', array_column($actorIdArray, '_id'))->get()->toArray();
        foreach($actorIdArray as $index => $p) {
            $key = array_search($p['_id'], array_column($listActorDetail, '_id'));
            $actorIdArray[$index]['detail'] = $listActorDetail[$key];
        }
        return $actorIdArray;
    }

    protected function getListDirectorDetail($directorIdArray)
    {
        $listDirectorDetail = $this->peopleRepository::whereIn('_id', $directorIdArray)->get()->toArray();
        foreach($directorIdArray as $index => $p) {
            $key = array_search($p, array_column($listDirectorDetail, '_id'));
            $directorIdArray[$index] = (array) $directorIdArray[$index];
            $directorIdArray[$index]['detail'] = $listDirectorDetail[$key];
        }
        return $directorIdArray;
    }

    protected function getListRibbon($titleId, $isActive = false)
    {
        if($isActive){
            $aggregate = [
                ['$unwind' => '$filmList'],
                ['$match' =>
                    [
                        'filmList._id' => $titleId,
                    ]
                ],
                ['$match' =>
                    [
                        'status' => 'active'
                    ]
                ],
                ['$sort' =>
                    ['filmList.order' => 1]
                ],
                ['$project' =>
                    [
                        '_id' => ['$toString' => '$_id'],
                        'order' => '$filmList.order',
                        'slug' => 1
                    ]
                ]
            ];
        }else{
            $aggregate = [
                ['$unwind' => '$filmList'],
                ['$match' =>
                    [
                        'filmList._id' => $titleId,
                    ]
                ]
            ];
        }
        
        $ribbon = $this->ribbonRepository::raw(function ($collection) use ($aggregate) {
            return $collection->aggregate($aggregate);
        })->toArray();
        return $ribbon;
    }

    protected function getListImage($imageArray)
    {
        $photo = [];
        foreach($imageArray as $platform => $item) {
            foreach ($item as $key => $value) {
               foreach ($value as $i => $r) {
                   array_push($photo,
                       [
                            'url'=>$r['url'],
                            'platform' => $platform,
                            'role' => $key,
                            'height' => $r['height'],
                            'width' => $r['width'],
                            'aspect' => $r['ratio'],
                            'id' => $r['_id'].'-'.$key.'-'.$r['status'].'-'.$platform,
                            '_id' => $r['_id'],
                            'status' => $r['status'],
                       ]);
               }
            }
        }
        return $photo;
    }

    protected function getListRelatedDetail($relatedIdArray)
    {
        $listRelatedDetail = $this->titleRepository::whereIn('_id', array_column($relatedIdArray, '_id'))->get()->toArray();
        foreach($relatedIdArray as $index => $r) {
            $key = array_search($r['_id'], array_column($listRelatedDetail, '_id'));
            $relatedIdArray[$index] = (array) $relatedIdArray[$index];
            $relatedIdArray[$index]['detail'] = $listRelatedDetail[$key];
        }
        return $relatedIdArray;
    }

    protected function getDamInfo($sourceId)
    {   
        $sourceDetail = $this->damRepository::where('_id', '=', $sourceId)->first()->toArray();
        return $sourceDetail;
    }

    public function update(Request $request)
    {
        // Update ribbon
        $listNewRibbonId = array_column($request->ribbon, '_id');
        $listOldRibbonId = array_column($this->getListRibbon($request->title['_id']), '_id');
        $listAddRibbon = array_diff($listNewRibbonId, $listOldRibbonId);
        $listDelRibbon = array_diff($listOldRibbonId, $listNewRibbonId);
        foreach ($listAddRibbon as $item) {
            $element = $this->ribbonRepository::find($item);
            $filmList = $element->filmList;

            array_push($filmList, [
                '_id' => $request->title['_id'],
                'order' => count($filmList)
            ]);
            $element->filmList = $filmList;
            $element->save();
        }

        foreach ($listDelRibbon as $item) {
            $element = $this->ribbonRepository::find($item);
            $filmList = $element->filmList;
            $index = array_search($request->title['_id'], array_column($filmList, '_id'));
            unset($filmList[$index]);
            $element->filmList = array_values($filmList);
            $element->save();
        }
        // Update title
        $title = $this->titleRepository->find($request->title['_id']);
        $result = $title->update($request->title);
        $formatedTitle = $this->formatQueueData($title->toArray());
        
        // push to Queue
        RabbitMQ::setExchange(Queue::EXCHANGE, Queue::EXCHANGE_TYPE);
        RabbitMQ::publishMessage($formatedTitle, Queue::ROUTING_KEY_TITLE_UPDATE); // convert to array before push to queue

        if ($result) {
            return response()->json(['status' => 'success', 'data' => $title], 200);
        } else {
            return response()->json(['status' => 'error', 'data' => 'Something went wrongs!'], 500);
        }
    }

    protected function formatQueueData($title)
    {
        $formatedTitle = $title;
        
        $formatedTitle['image'] = $this->formatQueuePhotoData($title);
        $formatedTitle['related'] = $this->formatQueueRelatedData($title);            
        $formatedTitle['actor'] = $this->formatQueuePeople($title, 'actor');
        $formatedTitle['ribbon'] = $this->getListRibbon($title['_id'], true);
        $formatedTitle['director'] = $this->formatQueuePeople($title, 'director');
        if (isset($formatedTitle['updatedAt'])) {
            $formatedTitle['updatedAt'] = (int) $formatedTitle['updatedAt'];
        }
        if (isset($formatedTitle['createdAt'])) {
            $formatedTitle['createdAt'] = (int) $formatedTitle['createdAt'];
        }

        return $formatedTitle;
    }

    protected function formatQueuePeople($title, $type)
    {
        $people = isset($title[$type]) ? $title[$type] : [];
        $delArray = [];
        foreach($people as $index => $p)
        {
            if ($type == 'director') {
                $people[$index] = $this->peopleRepository::find($p)->toArray();
            } else {
                $detail = isset($p['_id']) ? $this->peopleRepository::find($p['_id'])->toArray() : [];
                $people[$index] = array_merge($people[$index], $detail);
            }

            if (isset($people[$index]['status']) && (string) $people[$index]['status'] != 'active') {
                array_push($delArray, $index);
            }
        }

        foreach ($delArray as $index)
        {
            unset($people[$index]);
        }
        $people = array_values($people);

        return $people;
    }

    protected function formatQueueRelatedData($title)
    {
        $related = isset($title['related']) ? $title['related'] : [];
        $delArray = [];
        foreach($related as $index => $r)
        {
            $title = isset($r['_id']) ? $this->titleRepository::find($r['_id'])->toArray() : [];
            if(isset($title['status']) && (string) $title['status'] != 'active') {
                array_push($delArray, $index);
            } else {
                $minInfo = [];
                $minInfo['_id'] = isset($title['_id']) ? $title['_id'] : '';
                $minInfo['alternateName'] = isset($title['alternateName']) ? $title['alternateName'] : '';
                $minInfo['legacy_id'] = isset($title['legacy_id']) ? $title['legacy_id'] : '';
                $minInfo['name'] = isset($title['name']) ? $title['name'] : '';
                $minInfo['slug'] = isset($title['slug']) ? $title['slug'] : '';
                $photo = $this->formatQueuePhotoData($title);
                $minInfo['image'] = $photo;
                $minInfo['order'] = isset($r['order']) ? $r['order'] : '';
                $related[$index] = $minInfo;
            }
        }

        foreach($delArray as $index)
        {
            unset($related[$index]);
        }
        $related = array_values($related);

        return $related;
    }

    protected function formatQueuePhotoData($title)
    {
        $photo = isset($title['image']) ? $title['image'] : [];
        foreach($photo as $indexPlatform => $p)
        {
            foreach($p as $indexRole => $r)
            {
                foreach($r as $indexImage => $img)
                {
                    if(isset($img['status']) && (string) $img['status'] != 'active')
                    {
                        unset($photo[$indexPlatform][$indexRole][$indexImage]);
                    }
                }
                $photo[$indexPlatform][$indexRole] = array_values($photo[$indexPlatform][$indexRole]);
            }
        }

        return $photo;
    }

    //Upload Photo
    public function ajaxUploadImage(UploadImageRequest $request)
    {
        $originalName = $request->file('file')->getClientOriginalName();
        $extension = pathinfo($originalName, PATHINFO_EXTENSION);
        $hashValue = md5(file_get_contents($request->file('file')).env('PRIVATE_KEY'));
        $minioName = $request->name.'_'.$hashValue.'.'.$extension;
        $checksum = $this->checksumRepository->where('hashValue', '=', $hashValue)->first();
        $uploadStatus = true;

        if ($checksum == null) {
            Storage::disk('minio')->put($minioName, fopen($request->file('file'), 'r+'), '/public/uploads');
        } else {
            $minioName = $checksum->minioName;
            $uploadStatus = false;
        }

        $this->checksumRepository->create([
            'originalName'   => $originalName,
            'hashValue'     => $hashValue,
            'minioName'     => $minioName,
            'uploadStatus'  => $uploadStatus,
        ]);

        list($width, $height) = getimagesize($request->file);
        $id = (string) new ObjectId();
        if (isset($request->listPhoto)) {
            while (array_search($id, array_column(json_decode($request->listPhoto, true), '_id')) != false) {
                $id = (string) new ObjectId();
            }
        }

        return response()->json([
            'status' => 'success',
            'data' => [
                '_id' => $id,
                'legacy_id' => $request->legacy_id,
                'url' => $minioName,
                'height' => $height,
                'width' => $width,
                'ratio' => $request->ratio
            ]
        ], 200);
    }

    public function ajaxSearchRibbon(Request $request) {
        if ($request->ajax())
        {
            $ribbon = ($this->ribbonRepository::where('name', 'like', '%'.$request->keyword.'%')
            ->orWhere('alternateName', 'like', '%'.$request->keyword.'%'))->where('status','=','active')->get();
            return response()->json([
                'docs' => $ribbon
            ], 200);
        }
    }

    public function ajaxSearchActor(Request $request) {
        $people = $this->peopleRepository::where('name', 'like', '%'.$request->keyword.'%')->paginate(5);
        $result = [];
        $result['data'] = $people;
        return response()->json([
            'message' => 'send success!',
            'docs' => $result
        ], 200);
    }

    public function ajaxSearchDirector(Request $request) {
        $people = $this->peopleRepository::where('name', 'like', '%'.$request->keyword.'%')->paginate(5);
        $result = [];
        $result['data'] = $people;
        return response()->json([
            'message' => 'send success!',
            'docs' => $result
        ], 200);
    }

    public function getById(Request $request) {
        $title = $this->titleRepository::where('legacy_id', '=', $request->id)->first();
        return response()->json([
            'message' => 'send success!',
            'docs' => $title
        ], 200);
    }

    public function ajaxGetListDam()
    {
        $dams = $this->damRepository::all()->toArray();
        return response()->json([
            'total' => count($dams),
            'docs' => $dams
        ], 200);
    }

    public function ajaxFindDam(Request $request)
    {
        if ($request->id) {
            $dam = $this->getDamInfo($request->id);
            return response()->json(['status' => 'success', 'data' => $dam], 200);
        } else {
            return response()->json(['status' => 'error', 'data' => 'Something went wrongs!'], 500);
        }
        
    }

    public function ajaxChangeStatus(Request $request)
    {
        $title = $this->titleRepository::where('_id', '=', $request->id)->first();
        $title->status = $request->status;
        $title->save();

        return response()->json(['status' => 'success', 'data' => $title], 200);
    }
}
