<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Repositories\Backend\MongoDB\RibbonRepository;
use App\Repositories\Backend\MongoDB\PageRepository;
use Illuminate\Http\Request;
use MongoDB\BSON\ObjectId;
use PhpParser\Node\Stmt\DeclareDeclare;

use Kontoulis\RabbitMQLaravel\Facades\RabbitMQ;
use App\Helpers\Constants\Queue;

/**
 * Class ModuleController.
 */
class PageController extends Controller
{
    /**
     * @var ribbonRepository
     */
    protected $ribbonRepository;

    /**
     * @var pageRepository
     */
    protected $pageRepository;

    /**
     * RibbonController constructor.
     *
     * @param RibbonController $ribbonRepository
     */
    public function __construct(
        RibbonRepository $ribbonRepository,
        PageRepository $pageRepository
    ) {
        $this->ribbonRepository = $ribbonRepository->getModel();
        $this->pageRepository = $pageRepository->getModel();
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.page.index');
    }

    public function addNewTv() {
        return view('backend.page.addNewTv');
    }

    public function addNewWebsite() {
        return view('backend.page.addNewWebsite');
    }

    public function addNewMobile() {
        return view('backend.page.addNewMobile');
    }

    protected function formatListPageIndex($arrayPage)
    {
        $formatPage = [];
        foreach ($arrayPage as $page) {
            $element = [];
            $element['_id'] = isset($page['_id']) ? $page['_id'] : null;
            $element['name'] = isset($page['name']) ? $page['name'] : null;
            $element['titleEnglish'] = isset($page['titleEnglish']) ? $page['titleEnglish'] : null;
            $element['titleVietnamese'] = isset($page['titleVietnamese']) ? $page['titleVietnamese'] : null;
            array_push($formatPage, $element);
        }
        return $formatPage;
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchRibbon(Request $request) {
        if (isset($request->keyword)) {
            $ribbon = $this->ribbonRepository::where('name', 'like', '%'.$request->keyword.'%')
                ->where('ribbonType', '=', $request->ribbonType)->paginate(10);
            $result['ribbon'] = $ribbon;
            $result['ribbonType'] = $request->ribbonType;
            if (in_array($request->ribbonType, ['Landscape', 'Portrait'])) {
                switch ($request->position) {
                    case 'top':
                        $result['ribbonType'] = $result['ribbonType'].'Top';
                        break;
                    case 'middle':
                        $result['ribbonType'] = $result['ribbonType'].'Middle';
                        break;
                    case 'bottom':
                        $result['ribbonType'] = $result['ribbonType'].'Bottom';
                        break;
                }
            }
        } else {
            $result = [];
        }

        return response()->json([
            'message' => 'send success!',
            'data' => $result
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function findIdRibbon(Request $request) {
        $data = [];
        switch ($request->ribbonType) {
            case 'Spotlight':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'Original':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'LandscapeTop':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'LandscapeBottom':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'LandscapeMiddle':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'Sponsor':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'PortraitTop':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'PortraitMiddle':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'PortraitBottom':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
            case 'Preview':
                $data['ribbon'] = $this->ribbonRepository::find($request->id);
                break;
        }
        $result['data'] = $data;
        $result['ribbonType'] = $request->ribbonType;
        return response()->json([
            'message' => 'send success!',
            'data' => $result
        ], 200);
    }

    /**
     * @param Request $request
     */
    public function addPage(Request $request) {
        $page = $request->value;
        if ($page['status'] == false) {
            $page['status'] = 'unActive';
        } else {
            $page['status'] = 'active';
        }
        $page['ribbonList'] =  $request->page;

        $page = $this->pageRepository::create($page);

        $formatedPage = $this->formatQueuePage($page->toArray());

        // push to Queue
        RabbitMQ::setExchange(Queue::EXCHANGE, Queue::EXCHANGE_TYPE);
        RabbitMQ::publishMessage($formatedPage, Queue::ROUTING_KEY_PAGE_CREATE); // convert to array before push to queue

        return response()->json([
            'status' => 'success',
            'data' => $page
        ], 200);
    }

    private function formatQueuePage($page)
    {
        $formatedPage = $page;
        $arrayKey = [];
        if (isset($formatedPage['platform'])) {
            switch ($formatedPage['platform']) {
                case 'web':
                    $arrayKey = ['spotlight', 'landscapeTop', 'sponsor', 'landscapeMiddle', 'original', 'landscapeBottom'];
                    break;
                case 'tv':
                    $arrayKey = ['spotlight', 'landscapeTop', 'sponsor', 'landscapeMiddle', 'original', 'landscapeBottom'];
                    break;
                case 'mobile':
                    $arrayKey = ['spotlight', 'preview', 'portraitTop', 'sponsor', 'portraitMiddle', 'original', 'portraitBottom'];
                    break;
            }
        }

        $formatedPage['widgets'] = [];
        if (array_key_exists('ribbonList', $formatedPage)) {
            foreach ($arrayKey as $key) {
                if (array_key_exists ($key, $formatedPage['ribbonList']) && !empty($formatedPage['ribbonList'][$key])) {
                    foreach ($formatedPage['ribbonList'][$key] as $ribbon) {
                        $detail = isset($ribbon['_id']) ? $this->ribbonRepository::find($ribbon['_id'])->toArray() : [];
                        $element = [];
                        $element['_id'] = isset($detail['_id']) ? (string) $detail['_id'] : '';
                        $element['name'] = isset($detail['name']) ? $detail['name'] : '';
                        $element['alternateName'] = isset($detail['alternateName']) ? $detail['alternateName'] : '';
                        $element['ribbonType'] = isset($detail['ribbonType']) ? $detail['ribbonType'] : '';
                        $element['slug'] = isset($detail['slug']) ? $detail['slug'] : '';
                        $element['subTitle'] = isset($detail['subTitle']) ? $detail['subTitle'] : '';
                        $element['subTitleVn'] = isset($detail['subTitleVn']) ? $detail['subTitleVn'] : '';
                        array_push($formatedPage['widgets'], $element);
                    }
                }
            }
        }

        unset($formatedPage['ribbonList']);

        if (isset($formatedPage['updatedAt'])) {
            $formatedPage['updatedAt'] = (int) $formatedPage['updatedAt'];
        }
        if (isset($formatedPage['createdAt'])) {
            $formatedPage['createdAt'] = (int) $formatedPage['createdAt'];
        }

        return $formatedPage;
    }

    public function edit ($id) {
        $result = [];
        $page = $this->pageRepository::find($id);
        foreach ($page->ribbonList as $key => $idArray) {
            $element = [];
            $element[$key] = $this->ribbonRepository::whereIn('_id', array_column($idArray, '_id'))->get()->toArray();
            array_push($result, $element);
        }
        $ribbon = json_encode($result);
        return view('backend.page.edit')->with(compact(['page', 'ribbon']));
    }

    public function editWebsite ($id) {
        $result = [];
        $page = $this->pageRepository::find($id);
        foreach ($page->ribbonList as $key => $idArray) {
            $element = [];
            $element[$key] = $this->ribbonRepository::whereIn('_id', array_column($idArray, '_id'))->get()->toArray();
            array_push($result, $element);
        }
        $ribbon = json_encode($result);
        return view('backend.page.editWebsite')->with(compact(['page', 'ribbon']));
    }

    public function editMobile ($id) {
        $result = [];
        $page = $this->pageRepository::find($id);
        foreach ($page->ribbonList as $key => $idArray) {
            $element = [];
            $element[$key] = $this->ribbonRepository::whereIn('_id', array_column($idArray, '_id'))->get()->toArray();
            array_push($result, $element);
        }
        $ribbon = json_encode($result);
        return view('backend.page.editMobile')->with(compact(['page', 'ribbon']));
    }

    /**
     * @param $keyword
     * @return array
     */
    public function searchPage(Request $request) {
        if ($request->platform == 'website') {
            if (isset($request->keyword)) {
                $page = $this->formatListPageIndex($this->pageRepository::where('name', 'like', '%'.$request->keyword.'%')
                    ->where('platform', '=', $request->platform)->paginate(10));
            } else if (empty($request->keyword) || $request->keyword) {
                $page = $this->pageRepository::where('platform', '=', $request->platform)->paginate(10);
            }
        } else if ($request->platform == 'mobile') {
            if (isset($request->keyword)) {
                $page = $this->formatListPageIndex($this->pageRepository::where('name', 'like', '%'.$request->keyword.'%')
                    ->where('platform', '=', $request->platform)->paginate(10));
            } else if (empty($request->keyword) || $request->keyword) {
                $page = $this->pageRepository::where('platform', '=', $request->platform)->paginate(10);
            }
        } else {
            if (isset($request->keyword)) {
                $page = $this->formatListPageIndex($this->pageRepository::where('name', 'like', '%'.$request->keyword.'%')
                    ->where('platform', '=', $request->platform)->paginate(10));
            } else if (empty($request->keyword) || $request->keyword) {
                $page = $this->pageRepository::where('platform', '=', $request->platform)->paginate(10);
            }
        }

        $result = [];
        $result['data'] = $page;
        $result['total'] = count($page);
        return response()->json([
            'message' => 'send success!',
            'data' => $result
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchIdPage(Request $request) {
        $page = $this->pageRepository::where('_id', $request->id)->paginate(10);
        $result = [];
        $result['data'] = $page;
        $result['total'] = count($page);
        return response()->json([
            'message' => 'send success!',
            'data' => $page
        ], 200);
    }

    public function updatePage (Request $request) {
        $page = $request->value;
        if ($page['status'] == false) {
            $page['status'] = 'unActive';
        } else {
            $page['status'] = 'active';
        }
        $page['ribbonList'] =  $request->page;
        $pageList = $this->pageRepository->find($page['_id']);
        $result = $pageList->update($page);
        if ($result) {
            return response()->json(['status' => 'success', 'data' => $page], 200);
        } else {
            return response()->json(['status' => 'error', 'data' => 'Something went wrongs!'], 500);
        }
    }

}
