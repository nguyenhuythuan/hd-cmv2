<?php

namespace App\Http\Controllers\Backend;

use App\Imports\PeopleImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\MongoDB\TitleRepository;
use App\Repositories\Backend\MongoDB\PeopleRepository;
use MongoDB\BSON\ObjectId;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;
class PeopleController extends Controller
{
    /**
     * @var TitleRepository
     */
    protected $titleRepository;

    /**
     * @var PeopleRepository
     */
    protected $peopleRepository;

    public function __construct(
        TitleRepository $titleRepository,
        PeopleRepository $peopleRepository
    ) {
        $this->titleRepository = $titleRepository->getModel();
        $this->peopleRepository = $peopleRepository->getModel();
    }


    public function index()
    {
        return view('backend.people.index');
    }

    protected function formatListPeopleIndex($arrayPeople)
    {
        $formatPeople = [];
        foreach ($arrayPeople as $peoples) {
            $element = [];
            $element['_id'] = isset($peoples['_id']) ? $peoples['_id'] : null;
            $element['name'] = isset($peoples['name']) ? $peoples['name'] : null;
            $element['birthday'] = isset($peoples['birthday']) ? $peoples['birthday'] : null;
            $element['gender'] = isset($peoples['gender']) ? $peoples['gender'] : null;
            $element['nickname'] = isset($peoples['nickname']) ? $peoples['nickname'] : null;
            $element['image'] = isset($peoples['image']) ? $peoples['image'] : null;
            array_push($formatPeople, $element);
        }
        return $formatPeople;
    }

    /**
     * @param $keyword
     * @return array
     */
    public function searchPeople(Request $request) {
        if ($request->keyword) {
            $people = $this->formatListPeopleIndex($this->peopleRepository::where('name', 'like', '%'.$request->keyword.'%')->paginate(10));
        } else {
            $people = $this->peopleRepository::paginate(10);
        }
        $result = [];
        $result['data'] = $people;
        $result['total'] = count($people);
        return response()->json([
            'message' => 'send success!',
            'data' => $result
        ], 200);
    }

    public function searchIdPeople(Request $request) {
        $people = $this->peopleRepository::where('_id', $request->id)->paginate(10);
        $result = [];
        $result['data'] = $people;
        $result['total'] = count($people);
        return response()->json([
            'message' => 'send success!',
            'data' => $people
        ], 200);
    }

    public function addNew() {
        $people = $this->peopleRepository::paginate(10);
        return view('backend.people.addNew')->with(compact(['people']));
    }
    //Upload Photo
    public function uploadImage(Request $request)
    {
        if($request->hasFile('file')) {
            $file = $request->file('file')->getClientOriginalName();
            $extension = pathinfo($file, PATHINFO_EXTENSION);
            $target_file = md5_file($request->file('file'));
            $fileNameClient = $target_file.'.'.$extension;
            $checkFileServer = Storage::disk('minio')->has($fileNameClient);

            if ($checkFileServer) {
                $fileNameServer = Storage::disk('minio')->getMetadata($fileNameClient)['basename'];
            } else {
                Storage::disk('minio')->put($fileNameClient, fopen($request->file('file'), 'r+'), '/public/uploads');
                $fileNameServer = Storage::disk('minio')->getMetadata($fileNameClient)['basename'];
            }

            return response()->json([
                'status' => 'success',
                'data' => $fileNameServer
            ], 200);
        } else {
            return response()->json(['status' => 'error', 'data'=>'No file to upload!'], 500);
        }
    }
    public function addNewPeople(Request $request) {
        $resultData = $request->value;
        if (!isset($resultData['description'])) {
            $description = '';
        } else {
            $description = $resultData['description'];
        }
        if (!isset($resultData['nickname'])) {
            $nickname = '';
        } else {
            $nickname = $resultData['nickname'];
        }
        if (!isset($resultData['identifier'])) {
            $identifier = '';
        } else {
            $identifier = $resultData['identifier'];
        }
        if (!isset($resultData['birthday'])) {
            $birthday = '';
        } else {
            $birthday = $resultData['birthday'];
        }
        $people = $this->peopleRepository::create([
            '_id'           => new ObjectId(),
            'name'          => $resultData['name'],
            'image'         => $resultData['image'],
            'nickname'      => $nickname,
            'birthday'      => $birthday,
            'gender'        => $resultData['gender'],
            'description'   => $description,
            'identifier'    => $identifier,
            'status'        => 'active'
        ]);
        return response()->json([
            'status' => 'success',
            'data' => $people
        ], 200);
    }

    public function edit ($id) {
        $people = $this->peopleRepository::find($id);
        return view('backend.people.edit')->with(compact(['people']));
    }

    public function updatePeople (Request $request) {
        $people = $this->peopleRepository->find($request->value['_id']);
        $result = $people->update($request->value);
        if ($result) {
            return response()->json(['status' => 'success', 'data' => $people], 200);
        } else {
            return response()->json(['status' => 'error', 'data' => 'Something went wrongs!'], 500);
        }
    }

    public function import (Request $request) {
        $request->validate([
            'file' => 'required'
        ]);
        $result = [];
        $data = Excel::toArray(new PeopleImport, $request->file('file'));
        foreach ($data as $listPeople) {
            foreach ($listPeople as $key => $peoples) {
                $id = new ObjectId();
                if ($peoples['gender'] == 1) {
                    $peoples['gender'] = 'Male';
                } else if ($peoples['gender'] == 2) {
                    $peoples['gender'] = 'Female';
                } else {
                    $peoples['gender'] = 'Other';
                }
                $origDate = $peoples['birthday'];
                $date = str_replace('/', '-', $origDate);
                $newDate = date("Y-m-d", strtotime($date));
                $peoples['birthday'] = $newDate;
                $checkIdPeople = $this->peopleRepository::find($id);
                if ($checkIdPeople) {
                    return response()->json([
                        'status' => 'error',
                    ], 500);
                } else {
                    $peoples['_id'] = $id;
                    $result = $this->peopleRepository::create($peoples);
                }
            }
        }
        return response()->json([
            'status' => 'success',
            'data' => $result
        ], 200);
    }
}
