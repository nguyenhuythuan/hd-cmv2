<?php

namespace App\Http\Controllers\Backend;

use App\Imports\PeopleImport;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Backend\MongoDB\TitleRepository;
use App\Repositories\Backend\MongoDB\PeopleRepository;
use App\Repositories\Backend\MongoDB\RibbonRepository;
use MongoDB\BSON\ObjectId;
use Maatwebsite\Excel\Facades\Excel;
class RibbonController extends Controller
{
    /**
     * @var TitleRepository
     */
    protected $titleRepository;

    /**
     * @var PeopleRepository
     */
    protected $peopleRepository;

    /**
     * @var RibbonRepository
     */
    protected $ribbonRepository;

    public function __construct(
        TitleRepository $titleRepository,
        PeopleRepository $peopleRepository,
        RibbonRepository $ribbonRepository
    ) {
        $this->titleRepository = $titleRepository->getModel();
        $this->peopleRepository = $peopleRepository->getModel();
        $this->ribbonRepository = $ribbonRepository->getModel();
    }


    public function index()
    {
        return view('backend.ribbon.index');
    }

    /**
     * @param $keyword
     * @return array
     */
    public function searchRibbon(Request $request) {
        if ($request->keyword) {
            $ribbon = $this->ribbonRepository::where('name', 'like', '%'.$request->keyword.'%')->paginate(10);
        } else {
            $ribbon = $this->ribbonRepository::paginate(10);
        }
        $result = [];
        $result['data'] = $ribbon;
        $result['total'] = count($ribbon);
        return response()->json([
            'message' => 'send success!',
            'data' => $result
        ], 200);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function searchIdRibbon(Request $request) {
        $ribbon = $this->ribbonRepository::where('_id', $request->id)->get();
        return response()->json([
            'message' => 'send success!',
            'data' => $ribbon
        ], 200);
    }

    public function create()
    {
        return view('backend.ribbon.create');
    }

    public function ajaxGetTitleByConditions(Request $request)
    {
        $title = $this->queryTitleByConditions($request->mode, $request->conditions)->paginate(10)->toArray();
        return response()->json([
            'docs' => $this->formatTitleQueriedByConditions($title['data']),
            'total' => $title['total']
        ], 200);
    }

    public function ajaxCheckRibbonColumnExist(Request $request)
    {
        $ribbon = $this->ribbonRepository::where($request->column, 'like', $request->name)->get()->toArray();
        if (empty($ribbon)) 
        {
            return response()->json([
                'status' => 'success'
            ], 200);
        }
        else {
            return response()->json([
                'status' => 'error'
            ], 200);
        }
    }

    public function store(Request $request)
    {
        $filmList = [];
        if (isset($request->filter)) {
            $filmList = $this->formatTitleQueriedByConditions($this->queryTitleByConditions($request->filter['mode'], $request->filter['detail'])->get()->toArray());
        }
        
        $value = $request->all();
        $value['filmList'] = [];
        foreach($filmList as $index => $film) {
            array_push($value['filmList'], [
                '_id' => $film['_id'],
                'order' => $index + 1
            ]);
        }
        $ribbon = $this->ribbonRepository::create($value);
        return response()->json([
            'data' => $ribbon,
            'status' => 'success'
        ], 200);
    }

    private function queryTitleByConditions($mode, $conditions)
    {
        $title = $this->titleRepository;
        if ($mode == 'and') {
            foreach ($conditions as $condition) {
                switch ($condition['key']) {
                    case 'countryOfOrigin':
                        $title = $title->where($condition['key'], 'elemMatch', $condition['value']);
                        break;
                    case 'genre': 
                        $title = $title->whereIn($condition['key'], $condition['value']);
                        break;
                }
            }
            $title  = $title->where('status', '=', 'active');
        } else {
            foreach ($conditions as $condition) {
                switch ($condition['key']) {
                    case 'countryOfOrigin':
                        $title = $title->orWhere(function($query) use ($condition) {
                            $query->where($condition['key'], 'elemMatch', $condition['value'])
                                ->where('status', '=', 'active');
                        });
                        break;
                    case 'genre': 
                        $title = $title->orWhere(function($query) use ($condition) {
                            $query->whereIn($condition['key'], $condition['value'])
                                ->where('status', '=', 'active');
                        });
                        break;
                }
            }
        }
        return $title;
    }

    private function formatTitleQueriedByConditions($arrayTitle)
    {
        $formatTitle = [];
        foreach ($arrayTitle as $title) {
            $element = [];
            $element['_id'] = isset($title['_id']) ? $title['_id'] : null;
            $element['legacy_id'] = isset($title['legacy_id']) ? $title['legacy_id'] : null;
            $element['alternateName'] = isset($title['alternateName']) ? $title['alternateName'] : null;
            $element['priceType'] = isset($title['priceType']) ? $title['priceType'] : null;
            $element['type'] = isset($title['type']) ? $title['type'] : null;
            $element['videoQuality'] = isset($title['videoQuality']) ? $title['videoQuality'] : null;
            $element['status'] = isset($title['status']) ? $title['status'] : null;
            if (isset($title['image']['web']['thumbnail'])) {
                foreach($title['image']['web']['thumbnail'] as $img){
                    $element['thumbnail'] = $img['status'] == 'active' ? $img['url'] : null;
                }
            }

            array_push($formatTitle, $element);
        }
        return $formatTitle;
    }
}
