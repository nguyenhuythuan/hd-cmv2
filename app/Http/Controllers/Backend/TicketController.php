<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Auth\ticketCategory;
use App\Repositories\Backend\TicketRepository;
use Illuminate\Http\Request;

/**
 * Class ModuleController.
 */
class TicketController extends Controller
{
    /**
     * @var ticketRepository
     */
    protected $ticketRepository;

    /**
     * TicketRepository constructor.
     *
     * @param TicketRepository $ticketRepository
     */
    public function __construct(TicketRepository $ticketRepository)
    {
        $this->ticketRepository = $ticketRepository;
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('backend.ticket.index');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator
     */
    public function search(Request $request)
    {
        $paramSearch = $request->search;
        $paramDirection = $request->direction;
        $paramColumn = $request->column;
        $perPage = $request->perPage;
        $filterAll = $request->filter;
        $loadData = $this->ticketRepository;

        if ($paramSearch) {
           $loadData->where('id', '%'. strtolower($paramSearch) . '%', 'like')
               ->orWhere('createdByName', '%'. strtolower($paramSearch) . '%', 'like')
               ->orWhere('account', '%'. strtolower($paramSearch) . '%', 'like');
        }

        if (count((array) $filterAll) > 0)
        {
            foreach ($filterAll as $index => $value)
            {
                $object = json_decode($value);
                $loadData->where($object->name, $object->value, $object->operator);
            }
        }

        if ($paramColumn) {
            $loadData->orderBy($paramColumn,$paramDirection);
        }

        return $loadData->paginate(
            $perPage,
            [
                'id as uuid',
                'createdByName',
                'createdAt',
                'type',
                'status',
                'account',
                'platform',
                'categoryId'
            ],
            'page',
            null
        );
    }
}
