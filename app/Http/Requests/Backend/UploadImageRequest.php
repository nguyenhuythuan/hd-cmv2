<?php

namespace App\Http\Requests\Backend;

use App\Rules\RangeRatioRule;
use Illuminate\Foundation\Http\FormRequest;

class UploadImageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $photoConfig = config('role-photo.'.$this->input('platform').'.'.$this->input('role'));
        $rule = ['bail', 'mimes:jpeg,jpg,png,gif,svg'];
        if (in_array($this->input('platform'), ["tv", "web"]) && in_array($this->input('role'), ["titleSpotlightEn", "titleSpotlightVn"]) & !empty($photoConfig['maxWidth']) && !empty($photoConfig['height'])) {
            array_push($rule, 'dimensions:max_width='.$photoConfig['maxWidth'].',height='.$photoConfig['height']);
        } elseif (in_array($this->input('platform'), ["tv", "web"]) && in_array($this->input('role'), ["titleDetailEn", "titleDetailVn"]) & !empty($photoConfig['width']) && !empty($photoConfig['height'])) {
            array_push($rule, 'dimensions:width='.$photoConfig['width'].',height='.$photoConfig['height']);
        } elseif (!empty($photoConfig['minWidth'])) {
            array_push($rule, 'dimensions:min_width='.$photoConfig['minWidth']);
            array_push($rule, new RangeRatioRule($photoConfig['ratio']));
        }

        return [
            'file' => $rule
        ];
        return false;
    }
}
