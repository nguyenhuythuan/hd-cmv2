<?php

namespace App\Imports;

use App\Models\Backend\Mongo\People;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class PeopleImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        return new People([
            '_id'       => $row[0],
            'image'     => $row[1],
            'name'      => $row[2],
            'birthday'  => $row[3],
            'gander'    => $row[4],
            'nickname'  => $row[5],
        ]);
    }
}
