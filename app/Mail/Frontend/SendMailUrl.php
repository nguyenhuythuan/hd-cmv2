<?php

namespace App\Mail\Frontend;

use Illuminate\Http\Request;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

/**
 * Class SendContact.
 */
class SendMailUrl extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Request
     */
    public $request;

    /**
     * SendContact constructor.
     *
     * @param Request $request
     */
    public function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->to($this->request->toEmail)
            ->view('frontend.mail.sendMailUrl')
            ->subject(__('strings.emails.contact.subject', ['app_name' => app_name()]))
            ->from($this->request->toEmail, $this->request->name)
            ->replyTo($this->request->toEmail, $this->request->name);
    }
}
