<?php

namespace App\Models\Auth;

use App\Models\Traits\Uuid;
use OwenIt\Auditing\Auditable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use OwenIt\Auditing\Contracts\Auditable as AuditableInterface;

/**
 * Class Module.
 */
class BaseModule extends Authenticatable implements AuditableInterface
{
    use Auditable,
        Notifiable,
        Uuid;
}
