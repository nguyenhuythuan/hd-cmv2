<?php

namespace App\Models\Auth;

use OwenIt\Auditing\Auditable;
use Illuminate\Database\Eloquent\Model;
use OwenIt\Auditing\Contracts\Auditable as AuditableContract;

/**
 * Class SocialAccount.
 */
class DbModule extends Model implements AuditableContract
{
    use Auditable;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'modules';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'namevn',
        'nameModule',
        'subTileVn',
        'slug',
        'type',
        'transactionTip',
        'profile',
        'status',
    ];
}
