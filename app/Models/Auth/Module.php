<?php

namespace App\Models\Auth;

use App\Models\Auth\Traits\Attribute\ModuleAttribute;
use App\Models\Auth\Traits\Scope\ModuleScope;
use App\Models\Auth\Traits\Method\ModuleMethod;
use App\Models\Auth\Traits\Relationship\ModuleRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Module.
 */
class Module extends Model
{
    use ModuleAttribute,
        ModuleMethod,
        ModuleRelationship,
        ModuleScope;
}
