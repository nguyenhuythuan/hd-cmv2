<?php

namespace App\Models\Auth;
use App\Models\Auth\Traits\Relationship\ticketCategoryRelationship;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Module.
 */
class Ticket extends Model
{
    use ticketCategoryRelationship;

    protected $table = 'crm_tickets';
}
