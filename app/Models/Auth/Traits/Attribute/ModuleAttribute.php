<?php

namespace App\Models\Auth\Traits\Attribute;


/**
 * Trait ModuleAttribute.
 */
trait ModuleAttribute
{
    /**
     * @return string
     */
    public function getStatusButtonAttribute()
    {
        if ($this->isStatus()) {
            return '<span class="badge badge-success">'.__('buttons.general.crud.active').'</span>';
        }

        return '<span class="badge badge-danger">'.__('buttons.general.crud.unactive').'</span>';
    }

    /**
     * @return string
     */
    public function getTransactionTipButtonAttribute()
    {
        if ($this->isTransactionTip() == 1) {
            return '<span class="badge badge-success">'.__('buttons.general.crud.svod').'</span>';
        } elseif ($this->isTransactionTip() == 2) {
            return '<span class="badge badge-primary">'.__('buttons.general.crud.tvod').'</span>';
        } elseif ($this->isTransactionTip() == 3) {
            return '<span class="badge badge-info">'.__('buttons.general.crud.avod').'</span>';
        } else {
            return '<span class="badge badge-warning">'.__('buttons.general.crud.all').'</span>';
        }
    }
}
