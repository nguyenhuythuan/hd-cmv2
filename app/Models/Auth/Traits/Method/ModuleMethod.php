<?php

namespace App\Models\Auth\Traits\Method;

/**
 * Trait ModuleMethod.
 */
trait ModuleMethod
{
    /**
     * @return bool
     */
    public function isStatus()
    {
        return $this->status;
    }

    /**
     * @return bool
     */
    public function isTransactionTip()
    {
        return $this->transactionTip;
    }
}
