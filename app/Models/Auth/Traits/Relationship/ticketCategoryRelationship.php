<?php

namespace App\Models\Auth\Traits\Relationship;

use App\Models\Auth\ticketCategory;

/**
 * Class UserRelationship.
 */
trait ticketCategoryRelationship
{
    /**
     * @return mixed
     */
    public function categories()
    {
        return $this->hasMany(ticketCategory::class);
    }
}
