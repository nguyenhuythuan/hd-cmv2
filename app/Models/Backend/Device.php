<?php


namespace App\Models\Backend;

use App\Models\Backend\Traits\Attribute\DeviceAttribute;
use App\Models\Backend\Traits\Method\DeviceMethod;
use App\Models\Backend\Traits\Relationship\DeviceRelationship;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    use DeviceAttribute,
        DeviceMethod,
        DeviceRelationship;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'userId',
        'platform',
        'modelId',
        'modelName',
        'macAddress',
        'otherInfo',
        'createdAt',
        'updatedAt',
        'serialId'
    ];
}
