<?php


namespace App\Models\Backend\Mongo;

use App\Models\Backend\Mongo\Traits\Attribute\ChecksumAttribute;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;

class Checksum extends Eloquent
{
    use ChecksumAttribute;

    protected $connection = 'mongodb';
    protected $collection = 'checksum';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['_id'];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'originalName',
        'hashValue',
        'minioName',
        'uploadStatus',
    ];

    public static function boot() {
        parent::boot();

        // create a event to happen on creating
        static::creating(function($table)  {
            $table->createddBy = Auth::id();
        });

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updatedBy = Auth::id();
        });
    }
}
