<?php

namespace App\Models\Backend\Mongo;

// use Moloquent;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;


class Dam extends Eloquent
{
    
    protected $connection = 'mongodb';
    protected $collection = 'dam';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['_id'];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    protected $fillable = [
        'features',
        'markers',
        'name',
        'refName',
        'status',
        'type',
        'vcid',
        'videoItems'
    ];

    public static function boot()
    {
        parent::boot();

        // create a event to happen on creating
        static::creating(function ($table) {
            $table->createddBy = Auth::id();
        });

        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updatedBy = Auth::id();
        });
    }
}
