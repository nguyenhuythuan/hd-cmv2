<?php


namespace App\Models\Backend\Mongo;

// use Moloquent;
use App\Models\Backend\Mongo\Traits\Attribute\PageAttribute;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;


class Page extends Eloquent
{
    use PageAttribute;
    protected $connection = 'mongodb';
    protected $collection = 'page';
    public $timestamps = false;
    protected $guarded = ['_id'];
    protected $fillable = [
        '_id',
        'name',
        'platform',
        'slug',
        'titleEnglish',
        'titleVietnamese',
        'status',
        'ribbonList'
    ];

    public static function boot() {
        parent::boot();

        // create a event to happen on creating
        static::creating(function($table)  {
            $table->createdBy = Auth::id();
        });

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updatedBy = Auth::id();
        });
    }
}
