<?php


namespace App\Models\Backend\Mongo;

// use Moloquent;
use App\Models\Backend\Mongo\Traits\Attribute\PeopleAttribute;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;


class People extends Eloquent
{
    use PeopleAttribute;
    protected $connection = 'mongodb';
    protected $collection = 'people';
    protected $legacy_id = 'legacy_id';
    public $timestamps = false;
    protected $guarded = ['_id'];
    protected $fillable = [
        '_id',
        'name',
        'image',
        'status',
        'gender',
        'nickname',
        'birthday',
        'description',
        'identifier',
    ];

    public static function boot() {
        parent::boot();

        // create a event to happen on creating
        static::creating(function($table)  {
            $table->createddBy = Auth::id();
        });

        // create a event to happen on updating
        static::updating(function($table)  {
            $table->updatedBy = Auth::id();
        });
    }
}
