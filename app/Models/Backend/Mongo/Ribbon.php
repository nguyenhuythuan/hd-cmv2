<?php

namespace App\Models\Backend\Mongo;

// use Moloquent;
use App\Models\Backend\Mongo\Traits\Attribute\RibbonAttribute;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;


class Ribbon extends Eloquent
{
    use RibbonAttribute;

    protected $connection = 'mongodb';
    protected $collection = 'ribbon';
    protected $legacyId = 'legacy_id';

    const CREATED_AT = 'createdAt';
    const UPDATED_AT = 'updatedAt';

    /**
     * The attributes that aren't mass assignable.
     *
     * @var array
     */
    protected $guarded = ['_id'];

    /**
     * The storage format of the model's date columns.
     *
     * @var string
     */
    protected $dateFormat = 'U';

    protected $fillable = [
        'name',
        'subTitle',
        'subTitleVn',
        'status',
        'alternateName',
        'slug',
        'type',
        'options',
        'filmList',
        'displayType',
        'filter',
        'ribbonType'
    ];

    public static function boot()
    {
        parent::boot();

        // create a event to happen on creating
        static::creating(function ($table) {
            $table->createddBy = Auth::id();
        });

        // create a event to happen on updating
        static::updating(function ($table) {
            $table->updatedBy = Auth::id();
        });
    }
}
