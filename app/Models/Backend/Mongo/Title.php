<?php

namespace App\Models\Backend\Mongo;

// use Moloquent;
use App\Models\Backend\Mongo\Traits\Attribute\TitleAttribute;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Support\Facades\Auth;


class Title extends Eloquent
{
	use TitleAttribute;

	protected $connection = 'mongodb';
	protected $collection = 'title';
	protected $legacyId = 'legacy_id';

	const CREATED_AT = 'createdAt';
	const UPDATED_AT = 'updatedAt';

	/**
	 * The attributes that aren't mass assignable.
	 *
	 * @var array
	 */
	protected $guarded = ['_id'];

	/**
	 * The storage format of the model's date columns.
	 *
	 * @var string
	 */
	protected $dateFormat = 'U';


	/**
	 * The attributes that are mass assignable.
	 *
	 * @var array
	 */
	protected $fillable = [
		'actor',
        'publishScheduleOn',
		'aggregateRating',
		'contentRating',
		'alternateName',
		'copyrightStartOn',
		'copyrightExpireOn',
		'countryOfOrigin',
		'datePublished',
		'description',
		'director',
		'disambiguatingDescription',
		'genre',
        'subGenre',
		'image',
		'keyword',
		'legacy_id',
		'name',
		'numberOfSeasons',
		'price',
		'priceType',
		'productionCompany',
		'publisher',
		'slug',
		'spotline',
		'related',
		'containsSeason',
		'englishName',
		'video',
		'tags',
		'settings',
		'bindTo',
	];

	public static function boot() {
		parent::boot();

		// create a event to happen on creating
        static::creating(function($table)  {
            $table->createddBy = Auth::id();
        });

		// create a event to happen on updating
        static::updating(function($table)  {
            $table->updatedBy = Auth::id();
        });
	}
}
