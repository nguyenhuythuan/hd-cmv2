<?php

namespace App\Models\Backend\Mongo\Traits\Attribute;

/**
 * Trait TitleAttribute.
 */
trait PeopleAttribute
{
    public function setCreatedAt($value)
    {
        $this->attributes['createdAt'] = strtotime($value->toDateTime()->format('Y-m-d H:i:s'));
    }

    public function setUpdatedAt($value)
    {
        $this->attributes['updatedAt'] = strtotime($value->toDateTime()->format('Y-m-d H:i:s'));
    }

    public function setBirthdayAttribute($value)
    {
        $value != null ? $this->attributes['birthday'] = strtotime($value) : $this->attributes['birthday'] = 0;
    }
}
