<?php

namespace App\Models\Backend\Mongo\Traits\Attribute;

/**
 * Trait TitleAttribute.
 */
trait TitleAttribute
{
    public function setCreatedAt($value)
	{
		$this->attributes['createdAt'] = strtotime($value->toDateTime()->format('Y-m-d H:i:s'));
	}

	public function setUpdatedAt($value)
	{
		$this->attributes['updatedAt'] = strtotime($value->toDateTime()->format('Y-m-d H:i:s'));
	}

	public function setCopyrightStartOnAttribute($value)
	{
		$value != null ? $this->attributes['copyrightStartOn'] = strtotime($value) : $this->attributes['copyrightStartOn'] = 0;
	}

	public function setCopyrightExpireOnAttribute($value)
	{
		$value != null ? $this->attributes['copyrightExpireOn'] = strtotime($value) : $this->attributes['copyrightExpireOn'] = 0;
    }

    public function setPublishScheduleOnAttribute($value)
    {
        $value != null ? $this->attributes['publishScheduleOn'] = strtotime($value) : $this->attributes['publishScheduleOn'] = 0;
    }
}
