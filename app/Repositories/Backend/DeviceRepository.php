<?php


namespace App\Repositories\Backend;


use App\Models\Backend\Device;
use App\Repositories\BaseRepository;

/**
 * Class DeviceRepository
 * @package App\Repositories\Backend
 */
class DeviceRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Device::class;
    }


}
