<?php


namespace App\Repositories\Backend\MongoDB;


use App\Models\Backend\Mongo\Checksum;
use App\Repositories\MongoDBBaseRepository;

class ChecksumRepository extends MongoDBBaseRepository
{
    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Checksum::class;
    }
}
