<?php


namespace App\Repositories\Backend\MongoDB;

use App\Models\Backend\Mongo\Dam;
use App\Repositories\MongoDBBaseRepository;

class DamRepository extends MongoDBBaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Dam::class;
    }
}
