<?php


namespace App\Repositories\Backend\MongoDB;

use App\Models\Backend\Mongo\Page;
use App\Repositories\MongoDBBaseRepository;

class PageRepository extends MongoDBBaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Page::class;
    }
}
