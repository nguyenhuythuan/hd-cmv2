<?php


namespace App\Repositories\Backend\MongoDB;

use App\Models\Backend\Mongo\People;
use App\Repositories\MongoDBBaseRepository;

class PeopleRepository extends MongoDBBaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return People::class;
    }
}
