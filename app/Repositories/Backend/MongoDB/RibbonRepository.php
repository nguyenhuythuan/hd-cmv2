<?php


namespace App\Repositories\Backend\MongoDB;

use App\Models\Backend\Mongo\Ribbon;
use App\Repositories\MongoDBBaseRepository;

class RibbonRepository extends MongoDBBaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Ribbon::class;
    }
}
