<?php


namespace App\Repositories\Backend\MongoDB;


use App\Models\Backend\Mongo\Title;
use App\Repositories\MongoDBBaseRepository;

class TitleRepository extends MongoDBBaseRepository
{

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    public function model()
    {
        return Title::class;
    }
}
