<?php

namespace App\Repositories\Backend;

use App\Models\Auth\Ticket;
use App\Repositories\BaseRepository;

/**
 * Class ModuleRepository.
 */
class TicketRepository extends BaseRepository
{
    /**
     * @return string
     */
    public function model()
    {
        return Ticket::class;
    }
}
