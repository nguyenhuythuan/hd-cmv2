<?php

namespace  App\Repositories;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class MongoDBBaseRepository
 */
abstract class MongoDBBaseRepository
{
    /**
     * The repository model.
     *
     * @var \Jenssegers\Mongodb\Eloquent\Model
     */
    protected $model;

    /**
     * MongoDBBaseRepository constructor.
     */
    public function __construct()
    {
        return $this->makeModel();
    }

    /**
     * Specify Model class name.
     *
     * @return mixed
     */
    abstract public function model();

    /**
     * @return mixed
     */
    public function makeModel()
    {
        return $this->model = resolve($this->model());
    }

    /**
     * @return \Jenssegers\Mongodb\Eloquent\Model
     */
    public function getModel(): \Jenssegers\Mongodb\Eloquent\Model
    {
        return $this->model;
    }
}
