<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\Rule;

class RangeRatioRule implements Rule
{
    private $ratio;

    /**
     * Create a new rule instance.
     *
     * @param $ratio
     */
    public function __construct($ratio)
    {
        $this->ratio = $ratio;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if (!empty($this->ratio)) {
            list($width, $height) = getimagesize($value);
            $realRatio = round((float) $width/$height, 3);
            $min = round($this->ratio - 0.001, 3);
            $max = round($this->ratio + 0.001, 3);

            if (($realRatio >= $min) && ($realRatio <= $max)) {
                return true;
            }
            return false;
        }
        return true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'The :attribute has invalid image dimensions.';
    }
}
