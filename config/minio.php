<?php

return [
    'endpoint'              => env('MINIO_ENDPOINT'),
    'key'                   => env('MINIO_KEY'),
    'secret'                => env('MINIO_SECRET'),
    'region'                => env('MINIO_REGION'),
    'bucket'                => env('MINIO_BUCKET'),
    'uploadRoot'            => env('MINIO_ENDPOINT') . '/' . env('MINIO_BUCKET').'/',
    'image'                 => [
        'root'              => env('IMAGE_ROOT'),
        'original'          => env('IMAGE_ROOT') . '/plain/',
        'resize'            => [
            'w300'      => env('IMAGE_ROOT') . '/w300/',
            'w600'      => env('IMAGE_ROOT') . '/w600/',
            'w900'      => env('IMAGE_ROOT') . '/w900/',
        ]
    ],
];
