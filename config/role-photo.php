<?php 
return [
    'web'  => [
        'posterPortrait'    => [
            'minWidth'          => 209,
            'minHeight'         => 315,
            'ratio'             => (float) 2/3,
        ],
        'posterLandscape'    => [
            'minWidth'          => 329,
            'minHeight'         => 185,
            'ratio'             => (float) 16/9,
        ],
        'posterOriginal'    => [
            'minWidth'          => 1157,
            'minHeight'         => 651,
            'ratio'             => (float) 16/9,
        ],
        'posterSponsor'    => [
            'minWidth'          => 1001,
            'minHeight'         => 563,
            'ratio'             => (float) 16/9,  
        ],
        'banner'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'backdrop'    => [
            'minWidth'          => 1197,
            'minHeight'         => 763,
            'ratio'             => (float) 16/9,
        ],
        'thumbnail'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'preview'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'spotlight'    => [
            'minWidth'          => 1920,
            'minHeight'         => 1080,
            'ratio'             => (float) 16/9,
        ],
        'titleSpotlightEn'    => [
            'minWidth'          => '',
            'maxWidth'          => 650,
            'minHeight'         => '',
            'height'            => 260,
            'ratio'             => '',
        ],
        'titleSpotlightVn'    => [
            'minWidth'          => '',
            'maxWidth'          => 650,
            'minHeight'         => '',
            'height'            => 260,
            'ratio'             => '',
        ],
        'titleDetailEn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'width'             => 600,
            'height'            => 130,
            'ratio'             => '',
        ],
        'titleDetailVn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'width'             => 600,
            'height'            => 130,
            'ratio'             => '',
        ],
        'titleOriginalEn'    => [
            'minWidth'          => '',
            'maxWidth'          => 305,
            'minHeight'         => '',
            'width'             => '',
            'height'            => 122,
            'ratio'             => '',
        ],
        'titleOriginalVn'    => [
            'minWidth'          => '',
            'maxWidth'          => 305,
            'minHeight'         => '',
            'width'             => '',
            'height'            => 122,
            'ratio'             => '',
        ],
        'episode'    => [
            'minWidth'          => 329,
            'minHeight'         => 185,
            'ratio'             => (float) 16/9,
        ],
        'trailer'    => [
            'minWidth'          => 329,
            'minHeight'         => 185,
            'ratio'             => (float) 16/9,
        ],
    ],
    'tv'  => [
        'posterPortrait'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => (float) 2/3,
        ],
        'posterLandscape'    => [
            'minWidth'          => 346,
            'minHeight'         => 194,
            'ratio'             => (float) 16/9,
        ],
        'posterOriginal'    => [
            'minWidth'          => 1244,
            'minHeight'         => 700,
            'ratio'             => (float) 16/9,
        ],
        'posterSponsor'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => (float) 16/9,
        ],
        'banner'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'backdrop'    => [
            'minWidth'          => 1536,
            'minHeight'         => 864,
            'ratio'             => (float) 16/9,
        ],
        'thumbnail'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'preview'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'spotlight'    => [
            'minWidth'          => 1920,
            'minHeight'         => 1080,
            'ratio'             => (float) 16/9,
        ],
        'titleSpotlightEn'    => [
            'minWidth'          => '',
            'maxWidth'          => 650,
            'minHeight'         => '',
            'height'            => 260,
            'ratio'             => '',
        ],
        'titleSpotlightVn'    => [
            'minWidth'          => '',
            'maxWidth'          => 650,
            'minHeight'         => '',
            'height'            => 260,
            'ratio'             => '',
        ],
        'titleDetailEn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'width'             => 600,
            'height'            => 130,
            'ratio'             => '',
        ],
        'titleDetailVn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'width'             => 600,
            'height'            => 130,
            'ratio'             => '',
        ],
        'titleOriginalEn'    => [
            'minWidth'          => '',
            'maxWidth'          => 305,
            'minHeight'         => '',
            'width'             => '',
            'height'            => 122,
            'ratio'             => '',
        ],
        'titleOriginalVn'    => [
            'minWidth'          => '',
            'maxWidth'          => 305,
            'minHeight'         => '',
            'width'             => '',
            'height'            => 122,
            'ratio'             => '',
        ],
        'episode'    => [
            'minWidth'          => 346,
            'minHeight'         => 194,
            'ratio'             => (float) 16/9,
        ],
        'trailer'    => [
            'minWidth'          => 346,
            'minHeight'         => 194,
            'ratio'             => (float) 16/9,
        ],
    ],
    'mobile'  => [
        'posterPortrait'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'posterLandscape'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'posterOriginal'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'posterSponsor'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'banner'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'backdrop'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'thumbnail'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'preview'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'spotlight'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'titleSpotlightEn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'titleSpotlightVn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'titleDetailEn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'titleDetailVn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'titleOriginalEn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'titleOriginalVn'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'episode'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
        'trailer'    => [
            'minWidth'          => '',
            'minHeight'         => '',
            'ratio'             => '',
        ],
    ]
];
