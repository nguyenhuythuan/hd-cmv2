<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

/**
 * Class PermissionRoleTableSeeder.
 */
class PermissionRoleTableSeeder extends Seeder
{
    use DisableForeignKeys;

    /**
     * Run the database seed.
     */
    public function run()
    {
        $this->disableForeignKeys();

        // Create Roles
        $admin = Role::create(['name' => config('access.users.admin_role')]);
        $executive = Role::create(['name' => 'executive']);
        $user = Role::create(['name' => config('access.users.default_role')]);

        // Create Permissions
        $permissions = [
            ['name' => 'view backend', 'description' => 'View Admin Backend'],
            ['name' => 'view device', 'description' => 'View Device list'],
            ['name' => 'view module', 'description' => 'View Module List'],
            ['name' => 'view customer service', 'description' => 'View Customer Service'],
            ['name' => 'view movie', 'description' => 'View Movies Content']
        ];
        foreach ($permissions as $permission) {
            Permission::create(['name' => $permission['name'], 'description' => $permission['description']]);
        }

        // ALWAYS GIVE ADMIN ROLE ALL PERMISSIONS
        $admin->givePermissionTo(Permission::all());

        // Assign Permissions to other Roles
        $executive->givePermissionTo('view backend');

        $this->enableForeignKeys();
    }
}
