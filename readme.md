## Base on Laravel 5.8

[![Latest Stable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/stable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate)
[![Latest Unstable Version](https://poser.pugx.org/rappasoft/laravel-5-boilerplate/v/unstable)](https://packagist.org/packages/rappasoft/laravel-5-boilerplate) 
<br/>
[![StyleCI](https://styleci.io/repos/30171828/shield?style=plastic)](https://styleci.io/repos/30171828/shield?style=plastic)
[![CircleCI](https://circleci.com/gh/rappasoft/laravel-5-boilerplate/tree/master.svg?style=svg)](https://circleci.com/gh/rappasoft/laravel-5-boilerplate/tree/master)
<br/>
![GitHub contributors](https://img.shields.io/github/contributors/rappasoft/laravel-5-boilerplate.svg)
![GitHub stars](https://img.shields.io/github/stars/rappasoft/laravel-5-boilerplate.svg?style=social)


### Official Documentation

[Click here for the official documentation](http://laravel-boilerplate.com)


### Introduction

Content Manager of Fim+ sourcecode

### How to install
This package ships with a .env.example file in the root of the project.

- You must rename this file to just .env
- run composer install
- run npm install
- You must create your database on your server and on your .env file update the following lines:
 
  DB_CONNECTION=mysql
  DB_HOST=127.0.0.1
  DB_PORT=3306
  DB_DATABASE=homestead
  DB_USERNAME=homestead
  DB_PASSWORD=secret
- run php artisan key:generate (then insert key to .env)  
- run php artisan migrate
- run php artisan db:seed (to generate data)  
- run php artisan storage:link
- run npm run dev (to extract scss file)

