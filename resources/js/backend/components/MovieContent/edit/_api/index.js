const uploadImage = async function(formData) {
  const url = '/admin/movie/uploadImage';
  const config = {
    headers: {
      "Content-Type": "multipart/form-data"
    }
  };
  const response = await axios.post(url, formData, config);
  return response;
}

const searchActorApi = async function(config) {
  const url = '/admin/movie/searchActor';
  const response = await axios.get(url, config);
  return response;
}

const searchDirectorApi = async function(config) {
  const url = '/admin/movie/searchDirector';
  const response = await axios.get(url, config);
  return response;
}

const saveTitle = async function(data) {
  const url = '/admin/movie/update';
  const response = await axios.post(url, data);
  return response;
}

const getDamInfoApi = async function(data) {
  const url = '/admin/movie/findDam';
  const response = await axios.get(url, data);
  return response;
}

const changeTitleStatusApi = async function(data) {
  const url = '/admin/movie/changeStatus';
  const response = await axios.post(url, data);
  return response;
}

export default {
  uploadImage,
  searchActorApi,
  searchDirectorApi,
  saveTitle,
  getDamInfoApi,
  changeTitleStatusApi,
}