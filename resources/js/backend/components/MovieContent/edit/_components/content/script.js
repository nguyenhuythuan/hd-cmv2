import {
	Form, Input, Select, DatePicker, Skeleton,
	Radio, AutoComplete, Table, Tooltip, Button
} from 'ant-design-vue'
import { mapGetters, mapActions } from 'vuex'
import moment from 'moment'

window.Vue.use(Form)
window.Vue.use(Input)
window.Vue.use(Select)
window.Vue.use(DatePicker)
window.Vue.use(Skeleton)
window.Vue.use(Radio)
window.Vue.use(AutoComplete)
window.Vue.use(Table)
window.Vue.use(Tooltip)
window.Vue.use(Button)

export default {
	components: {
		'content-actor': () => import(/* webpackChunkName: "js/chunks/content-actor.chunk" */'./actor/index.vue'),
		'content-director': () => import(/* webpackChunkName: "js/chunks/content-director.chunk" */'./director/index.vue'),
		'content-setting': () => import(/* webpackChunkName: "js/chunks/content-setting.chunk" */'./setting/index.vue'),
		'content-source-modal': () => import(/* webpackChunkName: "js/chunks/content-modal-source.chunk" */'./modal_source/index.vue'),
	},
	data() {
		return {
			moment: moment,
			types: [{ name: "tvseries" }, { name: "movie" }],
			ages: [{ name: "P - Thích hợp mọi độ tuổi" }, { name: "7+" }, { name: "13+" }, { name: "16+" }, { name: "18+" }],
			genres: {},
			sourceModalVisible: false,
			listDam: [],
			loadingSource: true
		}
	},
	computed: {
		...mapGetters("config", [
			"allCountries",
			"allGenres",
			"allPublishers",
			"allStudio"
		]),
		...mapGetters({
			title: '$_title/title',
			damInfo: '$_title/damInfo',
		}),
	},
	methods: {
		...mapActions({
			mergeData: '$_title/mergeData',
			setDataField: '$_title/setDataField',
			pullDamInfo: '$_title/pullDamInfo',
			getDamInfoApi: "$_title/getDamInfoApi"
		}),
		checkIsNumber(rule, value, callback) {
			if (!isNaN(value) || typeof value === undefined) {
				callback();
				return;
			}
			callback('The value must be a number');
		},
		checkGenreLength(rule, value, callback) {
			if (value.length > 1) {
				callback('You must select only one genre')
			} else {
				callback()
			}
		},
		checkKeyword(rule, value, callback) {
			let bool = true
			if (rule.field === 'searchKeywords') {
				_.each(value, function (i) {
					if (i.length > 20) {
						bool = false
						return false
					}
				})
				if (!bool) {
					callback('SearchKeywords length must be less than 20')
				} else {
					callback()
				}
			} else if (rule.field === 'franchise') {
				_.each(value, function (i) {
					if (i.length > 20) {
						bool = false
						return false
					}
				})
				if (!bool) {
					callback('Franchise length must be less than 20')
				} else {
					callback()
				}
			} else if (rule.field === 'remakeOf') {
				_.each(value, function (i) {
					if (i.length > 20) {
						bool = false
						return false
					}
				})
				if (!bool) {
					callback('RemakeOf length must be less than 20')
				} else {
					callback()
				}
			} else {
				_.each(value, function (i) {
					if (i.length > 20) {
						bool = false
						return false
					}
				})
				if (!bool) {
					callback('RecKeyWord length must be less than 20')
				} else {
					callback()
				}
			}
		},
		getContainSeasion(array, type) {
			var data = []
			for (var list in array) {
				var i = array[list]
				if (type === "season") return i.seasonNumber
				else return i.numberOfEpisodes
			}
		},
		getCountryCode(arrayCountry) {
			for (let i in arrayCountry) {
				let country = arrayCountry[i]
				return country.code
			}
		},
		onSelectCountry(value) {
			let country = { ..._.filter(this.allCountries, { code: value }) }
			this.mergeData({
				countryOfOrigin: country
			})
		},
		onSelectSubGenre(values) {
			this.genres.main = this.allGenres.main
			if (values != undefined) {
				values.forEach(value => {
					this.genres.main = _.reject(this.genres.main, { name: value })
				})
			}
		},
		onSelectGenre(values) {
			this.genres.sub = this.allGenres.sub
			if (values != undefined) {
				values.forEach(value => {
					this.genres.sub = _.reject(this.genres.sub, { name: value })
				})
			}
		},
		countryFilter(input, option) {
			return option.componentOptions.children[0].text.toLowerCase().indexOf(input.toLowerCase()) >= 0
		},
		onEnterCountry(keyPressed) {
			if (keyPressed.key === 'Enter') {
				this.$refs.country.blur()
			}
		},
		disabledStartOn(startOnValue) {
			const expireOnValue = this.title.copyrightExpireOn;
			if (!startOnValue || !expireOnValue) {
				return false;
			}
			return startOnValue.valueOf() > expireOnValue.valueOf();
		},
		disabledExpireOn(expireOnValue) {
			const startOnValue = this.title.copyrightStartOn;
			if (!expireOnValue || !startOnValue) {
				return false;
			}
			return startOnValue.valueOf() >= expireOnValue.valueOf();
		},
		disabledPublishOn(publishOnValue) {
			const startOnValue = this.title.copyrightStartOn;
			const expireOnValue = this.title.copyrightExpireOn;
			if (!expireOnValue || !startOnValue) {
				return true;
			} else {
				return (
					publishOnValue.valueOf() < startOnValue.valueOf() ||
					publishOnValue.valueOf() > expireOnValue.valueOf()
				);
			}
		},
		showSourceModal() {
			axios
				.get("/admin/movie/getListDam")
				.then(response => {
					this.listDam = response.data.docs;
					this.loadingSource = false;
				})
      .catch(errors => {});
			this.sourceModalVisible = true;
		},
		hideSourceModal() {
			this.sourceModalVisible = false;
		},
		selectSourceModal(id) {
			this.setDataField({ key: 'bindTo', value: id });
			let config = {
				params: {
					id: id
				}
			};
			this.getDamInfoApi(config)
				.then((response) => {
					this.pullDamInfo(response.data.data);
					this.$store.commit('$_title/CHANGE_SAVE_STATUS', { status: false });
				})
				.catch((error) => {
					console.log(error);
				});
		},
		initTitleValue() {
			this.form.setFieldsValue({ alternateName: this.title.alternateName })

			if (typeof this.title.englishName !== 'undefined') {
				this.form.setFieldsValue({ englishName: this.title.englishName })
			} else {
				this.form.setFieldsValue({ englishName: this.title.name })
			}

			this.form.setFieldsValue({ englishName: this.title.englishName })
			this.form.setFieldsValue({ name: this.title.name })
			this.form.setFieldsValue({ publisher: this.title.publisher })
			this.form.setFieldsValue({
				countryOfOrigin: [{ code: this.getCountryCode(this.title.countryOfOrigin) }]
			})
			this.form.setFieldsValue({ datePublished: this.title.datePublished })
			this.form.setFieldsValue({ type: this.title.type })
			this.form.setFieldsValue({
				numberOfSeasons: this.title.numberOfSeasons
			})
			// this.form.setFieldsValue({containsSeason: {0:{numberOfEpisodes: this.getContainSeasion(this.movieData.containsSeason, 'episode')}}});
			this.form.setFieldsValue({
				productionCompany: this.title.productionCompany
			})
			this.form.setFieldsValue({ duration: this.title.duration })
			this.form.setFieldsValue({
				aggregateRating: {
					imdb: this.title.aggregateRating.imdb,
					fimplus: {
						ratingValue: this.title.aggregateRating.fimplus.ratingValue
					}
				}
			})
			this.form.setFieldsValue({ slug: this.title.slug })
			this.form.setFieldsValue({ genre: this.title.genre })
			this.form.setFieldsValue({ subGenre: this.title.subGenre })
			this.form.setFieldsValue({ description: this.title.description })
			this.form.setFieldsValue({
				disambiguatingDescription: this.title.disambiguatingDescription
			})
			this.form.setFieldsValue({
				spotline: this.title.spotline
			})
			this.form.setFieldsValue({
        keyword: {
          searchKeyword: this.title.keyword.searchKeyword,
          remakeOf:this.title.keyword.remakeOf,
          franchise:this.title.keyword.franchise,
          recKeyword:this.title.keyword.recKeyword,
        }
      })
			// this.form.setFieldsValue({priceType: this.title.priceType})
			// this.form.setFieldsValue({price: this.title.price})

			if (typeof this.title.copyrightExpireOn !== 'undefined' && this.title.copyrightExpireOn !== 0) {
				this.form.setFieldsValue({
					copyrightExpireOn: moment.unix(this.title.copyrightExpireOn)
				})
			}
			if (typeof this.title.copyrightStartOn !== 'undefined' && this.title.copyrightStartOn !== 0) {
				this.form.setFieldsValue({
					copyrightStartOn: moment.unix(this.title.copyrightStartOn)
				})
			}
			if (typeof this.title.publishScheduleOn !== 'undefined' && this.title.publishScheduleOn !== 0) {
				this.form.setFieldsValue({
					publishScheduleOn: moment.unix(this.title.publishScheduleOn)
				})
			}

			this.form.setFieldsValue({
				tags: this.title.tags
			})
		},
	},
	beforeCreate() {
		let self = this
		this.form = this.$form.createForm(this, {
			onFieldsChange(props, fields) {
					self.$store.commit('$_title/CHANGE_SAVE_STATUS', {status: false});
			},
			onValuesChange(dom, values) {
				switch (Object.keys(values)[0]) {
					case 'genre':
						self.setDataField({ key: 'genre', value: values['genre'], })
						break
					case 'subGenre':
						self.setDataField({ key: 'subGenre', value: values['subGenre'], })
						break
					case 'tags':
						self.setDataField({ key: 'tags', value: values['tags'], })
						break
					case 'productionCompany':
						self.setDataField({ key: 'productionCompany', value: values['productionCompany'], })
						break
					case 'type':
						self.$store.commit('$_title/CHANGE_TITLE_TYPE')
						self.mergeData(values)
						break
					case 'keyword':
						let element = {
							searchKeyword: self.form.getFieldValue('keyword.searchKeyword'),
							recKeyword: self.form.getFieldValue('keyword.recKeyword'),
							franchise: self.form.getFieldValue('keyword.franchise'),
							remakeOf: self.form.getFieldValue('keyword.remakeOf'),
						}
						switch (Object.keys(values.keyword)[0]) {
							case 'searchKeyword':
								element.searchKeyword = values.keyword.searchKeyword
								break
							case 'recKeyword':
								element.recKeyword = values.keyword.recKeyword
								break
							case 'franchise':
								element.franchise = values.keyword.franchise
								break
							case 'remakeOf':
								element.remakeOf = values.keyword.remakeOf
								break
						}
						self.setDataField({
							key: 'keyword',
							value: element,
						})
						break
					default:
						self.mergeData(values)
						break
				}
			},
		})
		this.settingForm = this.$form.createForm(this)
	},
	created() {
		this.genres = { ...this.allGenres }
		this.onSelectGenre(this.title.genre)
		this.onSelectSubGenre(this.title.subGenre)

		this.$store.subscribe((mutation, state) => {
			if (mutation.type == '$_title/CLICK_SAVE_TITLE') {
				this.form.validateFields((err, values) => {
					if (!err) {
						this.$store.commit('$_title/CHANGE_SAVE_STATUS', { status: true });
						this.$store.commit('$_title/VALIDATE_TITLE_SUCCESS');
					}
				});
			}
		})

		this.$store.subscribe((mutation, state) => {
			if (mutation.type == '$_title/RESET_TITLE') {
				this.initTitleValue()
				this.$store.commit('$_title/CHANGE_SAVE_STATUS', { status: true })
				this.$message.success('Reset completely!', 1)
			}
		})
	},
	mounted() {
		this.initTitleValue()
		this.$store.commit('$_title/CHANGE_SAVE_STATUS', { status: true })
	},
}