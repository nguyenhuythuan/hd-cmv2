import api from '../_api';

const pullData = ({ commit, state }, payload) => {
  commit('PULL_TITLE', payload.title);
  commit('PULL_ACTOR', payload.actor);
  commit('PULL_DIRECTOR', payload.director);
  commit('PULL_RIBBON', payload.ribbon);
  commit('PULL_RELATED', payload.related);
  commit('PULL_DAMINFO', payload.damInfo);
  commit('SORT_EPISODE_IN_SEASON');
};

const pullRelated = ({ commit, state }, payload) => {
  commit('PULL_RELATED', payload);
};

const pullRibbon = ({ commit, state }, payload) => {
  commit('PULL_RIBBON', payload);
};

const pullDamInfo = ({ commit, state }, payload) => {
  commit('PULL_DAMINFO', payload);
};

const mergeData = ({ commit, state }, payload) => {
  commit('MERGE_DATA', payload);
};

const setDataField = ({ commit, state }, payload) => {
  commit('SET_DATA_FIELD', payload);
};

const editSeason = ({ commit, state }, payload) => {
  commit('EDIT_SEASON', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const addSeason = ({ commit, state }, payload) => {
  commit('ADD_SEASON', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const editEpisode = ({ commit, state }, payload) => {
  commit('EDIT_EPISODE', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const addEpisode = ({ commit, state }, payload) => {
  commit('ADD_EPISODE', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const uploadImage = ({ commit, state }, payload) => {
  return api.uploadImage(payload)
};

const addImage = ({ commit, state }, payload) => {
  commit('UNACTIVE_ALL_IMAGE', payload);
  commit('ADD_IMAGE', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const activeImage = ({ commit, state }, payload) => {
  commit('UNACTIVE_ALL_IMAGE', payload);
  commit('ACTIVE_IMAGE', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const searchActorApi = ({ commit, state }, payload) => {
  return api.searchActorApi(payload)
};

const addActor = ({ commit, state }, payload) => {
  commit('ADD_ACTOR', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const removeActor = ({ commit, state }, payload) => {
  commit('REMOVE_ACTOR', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const searchDirectorApi = ({ commit, state }, payload) => {
  return api.searchDirectorApi(payload)
};

const addDirector = ({ commit, state }, payload) => {
  commit('ADD_DIRECTOR', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const removeDirector = ({ commit, state }, payload) => {
  commit('REMOVE_DIRECTOR', payload);
  commit('CHANGE_SAVE_STATUS', { status: false });
};

const saveTitle = ({ commit, state }, payload) => {
  return api.saveTitle(payload)
};

const getDamInfoApi = ({ commit, state }, payload) => {
  return api.getDamInfoApi(payload)
};

const changeTitleStatusApi = ({ commit, state }, payload) => {
  return api.changeTitleStatusApi(payload)
};

export default {
  pullData,
  pullRelated,
  pullRibbon,
  pullDamInfo,
  mergeData,
  setDataField,
  uploadImage,
  editSeason,
  addSeason,
  editEpisode,
  addEpisode,
  addImage,
  activeImage,
  searchActorApi,
  addActor,
  removeActor,
  searchDirectorApi,
  addDirector,
  removeDirector,
  saveTitle,
  getDamInfoApi,
  changeTitleStatusApi,
};
  