const title =  state => state.title;
const actor = state => state.actor;
const director = state => state.director;
const ribbon = state => state.ribbon;
const related = state => state.related;
const damInfo = state => state.damInfo;
const isSaved = state => state.isSaved;

export default {
  title,
  actor,
  director,
  ribbon,
  related,
  damInfo,
  isSaved,
};
