import getters from './getters';
import mutations from './mutations';
import actions from './actions';

const state = {
  title: {},
  actor: [],
  director: [],
  ribbon: [],
  related: [],
  damInfo: {},
  isSaved: false
};

export default {
  namespaced: true,
  state,
  getters,
  mutations,
  actions,
};
