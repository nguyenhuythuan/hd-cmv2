const PULL_TITLE = (state, title) => {
    state.title = title;
  };
  
  const MERGE_DATA = (state, data) => {
    _.merge(state.title, data)
  };
  
  const SET_DATA_FIELD = (state, data) => {
    state.title[data.key] = data.value;
  };
  
  const PULL_ACTOR = (state, actor) => {
    state.actor = actor
  };
      
  const PULL_DIRECTOR = (state, director) => {
    state.director = director
  };
  
  const PULL_RIBBON = (state, ribbon) => {
    state.ribbon = ribbon
  };
  
  const PULL_RELATED = (state, related) => {
    state.related = related
  };

  const PULL_DAMINFO = (state, damInfo) => {
    state.damInfo = damInfo
  };
      
  const SORT_EPISODE_IN_SEASON = (state, type = 'asc') => {
    if (state.title.containsSeason != undefined) {
      state.title.containsSeason.map((season, index, seasons) => {
        if (type === 'desc') {
          return season.episode.sort((a, b) => parseInt(b.episodeNumber) - parseInt(a.episodeNumber))
        } else {
          return season.episode.sort((a, b) => parseInt(a.episodeNumber) - parseInt(b.episodeNumber))
        }
      })
    }
  };

  const SET_TRAILER_THUMBNAIL = (state, payload) => {
    state.title.video[payload.index].thumbnail = payload.data;
  };

  const EDIT_SEASON = (state, payload) => {
    state.title.containsSeason[payload.seasonIndex] = payload.seasonData;
  };

  const ADD_SEASON = (state, payload) => {
    state.title.containsSeason.push(payload.seasonData);
  };

  const EDIT_EPISODE = (state, payload) => {
    state.title.containsSeason[payload.seasonIndex].episode[payload.episodeIndex] = payload.episodeData;
  };

  const ADD_EPISODE = (state, payload) => {
    state.title.containsSeason[payload.seasonIndex].episode.push(payload.episodeData)
  };

  const ACTIVE_IMAGE = (state, payload) => {
    let index = _.findIndex(state.title.image[payload.platform][payload.role], {_id: payload.imageId});
    state.title.image[payload.platform][payload.role][index].status = 'active';
  };

  const UNACTIVE_ALL_IMAGE = (state, payload) => {
    state.title.image[payload.platform][payload.role] = _.map(state.title.image[payload.platform][payload.role], function(value, key, collection) {
      return (value.status = "unActive"), value;
    });
  };

  const ADD_IMAGE = (state, payload) => {
    state.title.image[payload.platform][payload.role].push(payload.imageData);
  };

  const ADD_ACTOR = (state, payload) => {
    state.actor.unshift(payload.detail);
    state.title.actor.push(payload.actor);
  };

  const REMOVE_ACTOR = (state, payload) => {
    state.title.actor.splice(payload.actorIndex, 1);
    state.actor.splice(payload.detailIndex, 1);
  };

  const ADD_DIRECTOR = (state, payload) => {
    state.director.unshift(payload.detail);
    state.title.director.push(payload.director);
  };

  const REMOVE_DIRECTOR = (state, payload) => {
    state.title.director.splice(payload.actorIndex, 1);
    state.director.splice(payload.detailIndex, 1);
  };

  const CLICK_SAVE_TITLE = (state, payload) => {
    return;
  };

  const CLICK_CANCEL_TITLE = (state, payload) => {
    return;
  };

  const CLICK_SUBMIT_TO_QC_TITLE = (state, payload) => {
    return;
  };

  const CLICK_PUBLISH_TITLE = (state, payload) => {
    return;
  };

  const CLICK_UNPUBLISH_TITLE = (state, payload) => {
    return;
  };

  const RESET_TITLE = (state, payload) => {
    return;
  };

  const VALIDATE_TITLE_SUCCESS = (state, payload) => {
    return;
  };

  const CHANGE_TITLE_TYPE = (state, payload) => {
    return;
  };

  const CHANGE_SAVE_STATUS = (state, payload) => {
    state.isSaved = payload.status;
  };
  
  export default {
    PULL_TITLE,
    MERGE_DATA,
    SET_DATA_FIELD,
    PULL_ACTOR,
    PULL_DIRECTOR,
    PULL_RIBBON,
    PULL_DAMINFO,
    PULL_RELATED,
    SORT_EPISODE_IN_SEASON,
    SET_TRAILER_THUMBNAIL,
    EDIT_SEASON,
    ADD_SEASON,
    EDIT_EPISODE,
    ADD_EPISODE,
    ACTIVE_IMAGE,
    UNACTIVE_ALL_IMAGE,
    ADD_IMAGE,
    ADD_ACTOR,
    REMOVE_ACTOR,
    ADD_DIRECTOR,
    REMOVE_DIRECTOR,
    CLICK_SAVE_TITLE,
    CLICK_CANCEL_TITLE,
    CLICK_SUBMIT_TO_QC_TITLE,
    CLICK_PUBLISH_TITLE,
    CLICK_UNPUBLISH_TITLE,
    RESET_TITLE,
    VALIDATE_TITLE_SUCCESS,
    CHANGE_TITLE_TYPE,
    CHANGE_SAVE_STATUS,
  };