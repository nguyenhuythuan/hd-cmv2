import Vue from 'vue'
import Vuex from 'vuex'
import tree from './modules/device'
import config from './modules/config'
import people from './modules/people'
import page from './modules/page'

Vue.use(Vuex)

export default new Vuex.Store({
  strict: true,
  modules: {
    tree,
    config,
    page,
    people
  }
})
