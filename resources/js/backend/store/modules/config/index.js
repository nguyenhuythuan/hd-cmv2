const state = {
  countries: [],
  genres: [],
  publishers: [],
  image:[]
}

const getters = {
  allCountries: state => {
    return state.countries
  },
  getCountryByCode: (state) => (code) => {
    return state.countries.find(country => country.code === code)
  },
  allGenres: state => {
    return state.genres
  },
  allPublishers: state => {
    return state.publishers
  },
  image: state=> {
    return state.image
  }
}

const actions = {}

const mutations = {
  IMPORT_COUNTRIES(state, countries) {
    state.countries = countries
  },
  IMPORT_GENRES(state, genres) {
    state.genres = genres
  },
  IMPORT_PUBLISHERS(state, publishers) {
    state.publishers = publishers
  },
  SET_IMAGE(state, image){
    state.image = image
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
