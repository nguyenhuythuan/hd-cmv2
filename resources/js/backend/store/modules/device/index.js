const state = {
    nodeDetail: [],
    nodeSelected: {}
}

const getters = {

}

const actions = {

}

const mutations = {
    ADD_NODE_DETAIL (state, detail) {
        state.nodeDetail = detail
    },
    SELECT_NODE (state, node) {
        state.nodeSelected = node
    }
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}
