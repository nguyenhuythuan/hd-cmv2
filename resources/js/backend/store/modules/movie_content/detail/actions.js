const pullData = ({ commit, state }, payload) => {
  commit('PULL_TITLE', payload.title);
  commit('PULL_ACTOR', payload.actor);
  commit('PULL_DIRECTOR', payload.director);
  commit('PULL_RIBBON', payload.ribbon);
  commit('PULL_RELATED', payload.related);
  commit('SET_INIT_FORM', payload.form);
  commit('SORT_EPISODE_IN_SEASON');
};

const mergeData = ({ commit, state }, payload) => {
  commit('MERGE_DATA', payload);
};

const setDataField = ({ commit, state }, payload) => {
  commit('SET_DATA_FIELD', payload);
};

export default {
  pullData,
  mergeData,
  setDataField,
};
  