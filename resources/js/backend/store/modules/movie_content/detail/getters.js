const title =  state => state.title;
const actor = state => state.actor;
const director = state => state.director;
const ribbon = state => state.ribbon;
const related = state => state.related;
const form = state => state.form;

export default {
  title,
  actor,
  director,
  ribbon,
  related,
  form,
};
