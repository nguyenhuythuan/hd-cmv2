const PULL_TITLE = (state, title) => {
  state.title = title;
};

const MERGE_DATA = (state, data) => {
  _.merge(state.title, data)
};

const SET_DATA_FIELD = (state, data) => {
  state.title[data.key] = data.value;
};

const PULL_ACTOR = (state, actor) => {
  state.actor = actor
};
    
const PULL_DIRECTOR = (state, director) => {
  state.director = director
};

const PULL_RIBBON = (state, ribbon) => {
  state.ribbon = ribbon
};

const PULL_RELATED = (state, related) => {
  state.related = related
};

const SET_INIT_FORM = (state, form) => {
  state.form = form
};
    
const SORT_EPISODE_IN_SEASON = (state, type = 'asc') => {
  if (state.title.containsSeason != undefined) {
    state.title.containsSeason.map((season, index, seasons) => {
      if (type === 'desc') {
        return season.episode.sort((a, b) => parseInt(b.episodeNumber) - parseInt(a.episodeNumber))
      } else {
        return season.episode.sort((a, b) => parseInt(a.episodeNumber) - parseInt(b.episodeNumber))
      }
    })
  }
};

export default {
  PULL_TITLE,
  MERGE_DATA,
  SET_DATA_FIELD,
  PULL_ACTOR,
  PULL_DIRECTOR,
  PULL_RIBBON,
  PULL_RELATED,
  SET_INIT_FORM,
  SORT_EPISODE_IN_SEASON,
};