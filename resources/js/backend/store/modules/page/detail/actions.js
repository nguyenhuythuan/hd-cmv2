const pullData = ({ commit, state }, payload) => {
    commit('PULL_PAGE', payload.page);
    commit('PULL_RIBBON', payload.ribbon);
};

const mergeData = ({ commit, state }, payload) => {
    commit('MERGE_DATA', payload);
};

const setDataField = ({ commit, state }, payload) => {
    commit('SET_DATA_FIELD', payload);
};

export default {
    pullData,
    mergeData,
    setDataField,
};
