const page =  state => state.page;
const ribbon = state => state.ribbon;

export default {
    page,
    ribbon
};
