const PULL_PAGE = (state, page) => {
    state.page = page;
};

const MERGE_DATA = (state, data) => {
    _.merge(state.page, data)
};

const SET_DATA_FIELD = (state, data) => {
    state.page[data.key] = data.value;
};

const PULL_RIBBON = (state, ribbon) => {
    state.ribbon = ribbon
};

export default {
    PULL_PAGE,
    MERGE_DATA,
    SET_DATA_FIELD,
    PULL_RIBBON,
};
