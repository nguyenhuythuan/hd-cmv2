const state = {
    people: {},
}

const getters = {
    people: state => {
        return state.people
    },
}

const actions = {
    pullData ({ commit, state }, payload) {
        commit('PULL_PEOPLE', payload.people)
    }
}

const mutations = {
    PULL_PEOPLE(state, people) {
        state.people = people
    },
}

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}

