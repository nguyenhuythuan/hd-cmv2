import '../bootstrap'
import '../plugins'
import Vue from 'vue'
import store from './store'

import { Message, notification } from 'ant-design-vue'
// import {
//     Avatar, TreeSelect, Rate, Breadcrumb, InputNumber, Steps, Skeleton,
//     Upload, Button, Layout, Table, Icon, Progress, Radio, Dropdown, Menu,
//     Carousel, Input, Calendar, Badge, Slider, Form, Tooltip, Select, Switch,
//     Tag, Affix, Spin, Alert, Checkbox, Tabs, Pagination, notification, Drawer, List,
//     Row, Col, Divider, Modal, Message, DatePicker, AutoComplete, Popconfirm
// } from 'ant-design-vue'

// Vue.use(Avatar)
// Vue.use(Button)
// Vue.use(Rate)
// Vue.use(TreeSelect)
// Vue.use(Breadcrumb)
// Vue.use(Layout)
// Vue.use(Table)
// Vue.use(Icon)
// Vue.use(Progress)
// Vue.use(Radio)
// Vue.use(Dropdown)
// Vue.use(Menu)
// Vue.use(Carousel)
// Vue.use(Input)
// Vue.use(Calendar)
// Vue.use(Badge)
// Vue.use(Slider)
// Vue.use(Form)
// Vue.use(Tooltip)
// Vue.use(Select)
// Vue.use(Tag)
// Vue.use(Affix)
// Vue.use(Spin)
// Vue.use(Alert)
// Vue.use(Checkbox)
// Vue.use(Tabs)
// Vue.use(Pagination)
// Vue.use(Upload)
// Vue.use(Message)
// Vue.use(Steps)
// Vue.use(InputNumber)
// Vue.use(Drawer)
// Vue.use(Switch)
// Vue.use(notification)
// Vue.use(List)
// Vue.use(Row)
// Vue.use(Col)
// Vue.use(Divider)
// Vue.use(Modal)
// Vue.use(DatePicker)
// Vue.use(AutoComplete)
// Vue.use(Popconfirm)
// Vue.use(Skeleton)

Vue.prototype.$notification = notification
Vue.prototype.$message = Message
Vue.config.productionTip = false
window.Vue = Vue

import Countries from '@/backend/data_config/country.json'
import Genres from '@/backend/data_config/genre.json'
import Publisher from '@/backend/data_config/publisher.json'
import Image from '@/backend/data_config/image.json'

const app = new Vue({
    el: '#app',
    components: {
        // Clean UI
        CuiFooter: () => import( /* webpackChunkName: "js/chunks/cui-footer.chunk" */ '@/backend/components/LayoutComponents/Footer/index.vue'),
        CuiTopbar: () => import( /* webpackChunkName: "js/chunks/cui-topbar.chunk" */ '@/backend/components/LayoutComponents/Topbar/index.vue'),
        CuiMenuLeft: () => import( /* webpackChunkName: "js/chunks/cui-menu-left.chunk" */ '@/backend/components/LayoutComponents/Menu/MenuLeft/index.vue'),
        CuiMenuTop: () => import( /* webpackChunkName: "js/chunks/cui-menu-left.chunk" */ '@/backend/components/LayoutComponents/Menu/MenuTop/index.vue'),
        CuiBreadcrumbs: () => import( /* webpackChunkName: "js/chunks/cui-breadcrumbs.chunk" */ '@/backend/components/LayoutComponents/Breadcrumbs/index.vue'),
        CuiSettings: () => import( /* webpackChunkName: "js/chunks/cui-settings.chunk" */ '@/backend/components/LayoutComponents/Settings/index.vue'),

        // Movie Content
        MovieContentList: () => import( /* webpackChunkName: "js/chunks/movie-content-list.chunk" */ '@/backend/components/MovieContent/list/index.vue'),
        MovieContentEdit: () => import( /* webpackChunkName: "js/chunks/movie-content-edit.chunk" */ '@/backend/components/MovieContent/edit/index.vue'),

        // Modal
        ModalSeason: () => import( /* webpackChunkName: "js/chunks/modal-season.chunk" */ '@/backend/components/ModalComponents/ModalSeason/index.vue'),

        //People
        PeopleList: () => import( /* webpackChunkName: "js/chunks/people-list.chunk" */ '@/backend/components/People/list/index.vue'),
        PeopleDetailAddNew: () => import( /* webpackChunkName: "js/chunks/people-detail-add-new.chunk" */ '@/backend/components/People/detail/addNew/index.vue'),
        PeopleDetailEdit: () => import( /* webpackChunkName: "js/chunks/people-detail-add-new.chunk" */ '@/backend/components/People/detail/edit/index.vue'),

        //Ribbon
        RibbonList: () => import( /* webpackChunkName: "js/chunks/ribbon-list.chunk" */ '@/backend/components/Ribbon/list/index.vue'),
        RibbonCreate: () => import( /* webpackChunkName: "js/chunks/ribbon-create.chunk" */ '@/backend/components/Ribbon/create/index.vue'),

        //Page
        PageList: () => import( /* webpackChunkName: "js/chunks/page.chunk" */ '@/backend/components/Page/index.vue'),
        PageDetailAddNew: () => import( /* webpackChunkName: "js/chunks/page-detail-add-new.chunk" */ '@/backend/components/Page/detail/addNew/index.vue'),
        PageDetailAddNewWebsite: () => import( /* webpackChunkName: "js/chunks/page-detail-add-new-website.chunk" */ '@/backend/components/Page/detail/addNewWebsite/index.vue'),
        PageDetailAddNewMobile: () => import( /* webpackChunkName: "js/chunks/page-detail-add-new-mobile.chunk" */ '@/backend/components/Page/detail/addNewMobile/index.vue'),
        PageDetailEdit: () => import( /* webpackChunkName: "js/chunks/page-detail-edit.chunk" */ '@/backend/components/Page/detail/edit/index.vue'),
        PageDetailEditWebsite: () => import( /* webpackChunkName: "js/chunks/page-detail-edit-website.chunk" */ '@/backend/components/Page/detail/editWebsite/index.vue'),
        PageDetailEditMobile: () => import( /* webpackChunkName: "js/chunks/page-detail-edit-mobile.chunk" */ '@/backend/components/Page/detail/editMobile/index.vue'),

        // Device
        Device: () => import( /* webpackChunkName: "js/chunks/device.chunk" */ '@/backend/components/Device/index.vue'),
        MyTab: () => import( /* webpackChunkName: "js/chunks/my-tab.chunk" */ '@/backend/components/MyTabComponent.vue'),

    },
    store,
    created() {
      store.commit('config/IMPORT_COUNTRIES', Countries)
      store.commit('config/IMPORT_GENRES', Genres)
      store.commit('config/IMPORT_PUBLISHERS', Publisher)
      store.commit('config/SET_IMAGE', Image)
    }
})
