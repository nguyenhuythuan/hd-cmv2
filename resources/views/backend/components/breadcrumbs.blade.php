@if($breadcrumbs)
    <div style="display: flex;">
        <nav class="cui-breadcrumbs cui-breadcrumbs-bg" style="flex:1;">
            <span class="font-size-18 d-block">
                <span class="text-muted">Home ·</span>

                @foreach($breadcrumbs as $breadcrumb)
                    @if($breadcrumb->url && !$loop->last)
                        <span class="text-muted"><a href="{{ $breadcrumb->url }}">{{ $breadcrumb->title }} ·</a></span>
                    @else
                        <strong>{{ $breadcrumb->title }}</strong>
                    @endif
                @endforeach
            </span>
        </nav>
        
        @yield('breadcrumbsButton')
    </div>
@endif
