<!DOCTYPE html>
<html lang="en">
@langrtl
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}" dir="rtl">
@else
    <html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
@endlangrtl
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <title>@yield('title', app_name())</title>
    <meta name="description" content="@yield('meta_description', 'Fimplus Content Manager')">
    <link href="{{ URL::asset('cleanui/components/dummy-assets/common/img/favicon.png') }}" rel="shortcut icon">
    <link href="https://fonts.googleapis.com/css?family=Nunito+Sans:400,400i,700,700i,900" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/fonts/font-icomoon/style.css')  }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/fonts/font-awesome/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('resources/fonts/font-linearicons/style.css') }}">

    <link rel="stylesheet" href="{{URL::asset('css/style.css')}}">
    {{ style(mix('css/backend.css')) }}

    @yield('meta')
    <!-- {{-- See https://laravel.com/docs/5.5/blade#stacks for usage --}} -->
    @stack('before-styles')
    <!-- Check if the language is set to RTL, so apply the RTL layouts -->
    <!-- Otherwise apply the normal LTR layouts -->
        {{-- {{ style(mix('css/backend.css')) }} --}}
    @stack('after-styles')

    <script src="{{ URL::asset('cleanui/vendors/jquery/dist/jquery.min.js') }}"></script>
{{--    <!-- CLEAN UI HTML ADMIN TEMPLATE MODULES-->--}}
{{--    <!-- v2.0.2 -->--}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('cleanui/components/core/common/core.cleanui.css') }}">
{{--    <link rel="stylesheet" type="text/css" href="{{ URL::asset('cleanui/components/menu-top/common/menu-top.cleanui.css') }}">--}}
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('cleanui/components/breadcrumbs/common/breadcrumbs.cleanui.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ URL::asset('cleanui/components/footer/common/footer.cleanui.css') }}">
{{--    <script src="{{ URL::asset('cleanui/components/menu-top/common/menu-top.cleanui.js') }}"></script>--}}

    <!-- PRELOADER STYLES-->
    <style>
        .cui-initial-loading {
            position: fixed;
            z-index: 99999;
            top: 0;
            left: 0;
            width: 100%;
            height: 100%;
            background-position: center center;
            background-repeat: no-repeat;
            background-image: url(data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iNDFweCIgIGhlaWdodD0iNDFweCIgIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDEwMCAxMDAiIHByZXNlcnZlQXNwZWN0UmF0aW89InhNaWRZTWlkIiBjbGFzcz0ibGRzLXJvbGxpbmciPiAgICA8Y2lyY2xlIGN4PSI1MCIgY3k9IjUwIiBmaWxsPSJub25lIiBuZy1hdHRyLXN0cm9rZT0ie3tjb25maWcuY29sb3J9fSIgbmctYXR0ci1zdHJva2Utd2lkdGg9Int7Y29uZmlnLndpZHRofX0iIG5nLWF0dHItcj0ie3tjb25maWcucmFkaXVzfX0iIG5nLWF0dHItc3Ryb2tlLWRhc2hhcnJheT0ie3tjb25maWcuZGFzaGFycmF5fX0iIHN0cm9rZT0iIzAxOTBmZSIgc3Ryb2tlLXdpZHRoPSIxMCIgcj0iMzUiIHN0cm9rZS1kYXNoYXJyYXk9IjE2NC45MzM2MTQzMTM0NjQxNSA1Ni45Nzc4NzE0Mzc4MjEzOCIgdHJhbnNmb3JtPSJyb3RhdGUoODQgNTAgNTApIj4gICAgICA8YW5pbWF0ZVRyYW5zZm9ybSBhdHRyaWJ1dGVOYW1lPSJ0cmFuc2Zvcm0iIHR5cGU9InJvdGF0ZSIgY2FsY01vZGU9ImxpbmVhciIgdmFsdWVzPSIwIDUwIDUwOzM2MCA1MCA1MCIga2V5VGltZXM9IjA7MSIgZHVyPSIxcyIgYmVnaW49IjBzIiByZXBlYXRDb3VudD0iaW5kZWZpbml0ZSI+PC9hbmltYXRlVHJhbnNmb3JtPiAgICA8L2NpcmNsZT4gIDwvc3ZnPg==);
            background-color: #f2f4f8;
        }
    </style>
    <script>
        $(document).ready(function () {
            $('.cui-initial-loading').delay(0).fadeOut(10)
        })
    </script>
</head>
