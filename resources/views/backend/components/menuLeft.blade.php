<nav class="cui-menu-left">
    <div class="cui-menu-left-trigger cui-menu-left-trigger-action"></div>
    <div class="cui-menu-left-handler">
        <div class="cui-menu-left-handler-icon"></div>
    </div>
    <div class="cui-menu-left-inner">
        <div class="cui-menu-left-logo">
            <a href="/">
                <img
                    class="cui-menu-left-logo-default"
                    src="{{ asset('img/backend/brand/logo.png') }}"
                    alt=""
                />
                <img
                    class="cui-menu-left-logo-toggled"
                    src="{{ asset('img/backend/brand/sygnet.svg') }}"
                    alt=""
                />
            </a>
        </div>
        <div class="cui-menu-left-scroll">
            <ul class="cui-menu-left-list cui-menu-left-list-root">
                <li class="cui-menu-left-item">
                    <a href="{{ route('admin.dashboard') }}">
                        <span class="cui-menu-left-icon icmn-home"></span>
                        <span class="cui-menu-left-title">@lang('menus.backend.sidebar.dashboard')</span>
                    </a>
                </li>

                @can('view device')
                    <li class="cui-menu-left-item">
                        <a href="{{ route('admin.device') }}">
                            <span class="cui-menu-left-icon fa fa-desktop"></span>
                            <span class="cui-menu-left-title">@lang('strings.backend.device.title')</span>
                        </a>
                    </li>
                @endcan

                @can('view module')
{{--                    <li class="cui-menu-left-item">--}}
{{--                        <a href="{{ route('admin.module') }}">--}}
{{--                            <span class="cui-menu-left-icon icmn-file-text"></span>--}}
{{--                            <span class="cui-menu-left-title">@lang('strings.backend.module.title')</span>--}}
{{--                        </a>--}}
{{--                    </li>--}}
                @endcan

                @can('view movie')
                    <li class="cui-menu-left-divider"></li>
                    <li class="cui-menu-left-item">
                        <a href="{{ route('admin.movie') }}">
                            <span class="cui-menu-left-icon fa fa-film"></span>
                            <span class="cui-menu-left-title">@lang('menus.backend.content.movie.title')</span>
                        </a>
                    </li>
                @endcan
                <li class="cui-menu-left-divider"></li>
                @if ($logged_in_user->isAdmin())
                    <li class="cui-menu-left-item cui-menu-left-submenu">
                        <a href="javascript: void(0);">
                            <span class="cui-menu-left-icon fa fa-user"></span>
                            <span class="cui-menu-left-title">@lang('menus.backend.access.title')</span>
                            @if ($pending_approval > 0)
                                <span class="ml-2 badge badge-danger">{{ $pending_approval }}</span>
                            @endif
                        </a>
                        <ul class="cui-menu-left-list">
                            <li class="cui-menu-left-item">
                                <a href="{{ route('admin.auth.user.index') }}">
                                    <span
                                        class="cui-menu-left-title">@lang('labels.backend.access.users.management')</span>
                                    @if ($pending_approval > 0)
                                        <span class="ml-2 badge badge-danger">{{ $pending_approval }}</span>
                                    @endif
                                </a>
                            </li>
                            <li class="cui-menu-left-item">
                                <a href="{{ route('admin.auth.role.index') }}">
                                    <span
                                        class="cui-menu-left-title">@lang('labels.backend.access.roles.management')</span>
                                </a>
                            </li>
                            <li class="cui-menu-left-item">
                                <a href="{{ route('admin.auth.permission.index') }}">
                                    <span
                                        class="cui-menu-left-title">@lang('labels.backend.access.permissions.management')</span>
                                </a>
                            </li>
                        </ul>
                    </li>
                @endif

                <li class="cui-menu-left-item cui-menu-left-submenu">
                    <a href="javascript: void(0);">
                        <span class="cui-menu-left-icon fa fa-list"></span>
                        <span class="cui-menu-left-title">@lang('menus.backend.log-viewer.main')</span>
                    </a>
                    <ul class="cui-menu-left-list">
                        <li class="cui-menu-left-item">
                            <a href="{{ route('log-viewer::dashboard') }}">
                                <span class="cui-menu-left-title"> @lang('menus.backend.log-viewer.dashboard')</span>
                            </a>
                        </li>
                        <li class="cui-menu-left-item">
                            <a href="{{ route('log-viewer::logs.list') }}">
                                <span class="cui-menu-left-title">@lang('menus.backend.log-viewer.logs')</span>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
