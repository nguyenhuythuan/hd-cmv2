@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.dashboard.title'))

@section('content')
    <div class="row">
        <div class="col">
            <div class="card">
                <div class="card-header">
                    <strong>@lang('strings.backend.dashboard.welcome') {{ $logged_in_user->name }}!</strong>
                </div><!--card-header-->
                <div class="card-body">
                    <a href="#" class="btn btn-square btn-primary">Primary</a>
                    <a href="#" class="btn btn-square btn-secondary">Secondary</a>
                    <a href="#" class="btn btn-square btn-success">Success</a>
                    <a href="#" class="btn btn-square btn-info">Info</a>
                    <a href="#" class="btn btn-square btn-warning">Warning</a>
                    <a href="#" class="btn btn-square btn-danger">Danger</a>
                </div><!--card-body-->
            </div><!--card-->
        </div><!--col-->
    </div><!--row-->
@endsection
