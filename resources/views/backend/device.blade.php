@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.device.title'))

@section('content')
    <!-- Scripts -->
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <div class="container" id="app">
        <div class="card">
            <div class="card-header">
                Device Manager
            </div>

            <div class="card-body row">
                <div class="col-md-4">
                    <device />
                </div>
                <div class="col-md-8">
                    <my-tab />
                </div>
            </div>
        </div><!--col-->
    </div><!--row-->
@endsection
