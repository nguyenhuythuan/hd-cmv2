<footer class="app-footer">
    <div>
        <strong>@lang('labels.general.copyright') &copy; {{ date('Y') }} Fimplus
        </strong> @lang('strings.backend.general.all_rights_reserved')
    </div>

{{--    <div class="ml-auto">Theme by <a href="http://coreui.io">CoreUI</a></div>--}}
</footer>
