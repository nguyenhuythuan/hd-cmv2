@include('backend.components.head')
<body class="cui-config-borderless cui-menu-colorful cui-menu-left-shadow">
<div class="cui-initial-loading"></div>
<div class="cui-layout cui-layout-has-sider" id="app">
{{--    @include('backend.components.menuRight')--}}
{{--    @include('backend.components.menuLeft')--}}
    <div class="cui-layout">
        <div>
{{--            @include('backend.components.topBar')--}}
            <cui-menu-top/>
        </div>
        @if(in_array(\Route::current()->getName(), ['admin.movie.edit', 'admin.season.edit']))
            <div class="cui-layout-content">
                <div class="cui-utils-content">
                    @yield('content')
                </div>
            </div>
        @else
            <div class="cui-layout-content">
                {{ Breadcrumbs::render() }}
                <div class="cui-utils-content">
                    @yield('content')
                </div>
            </div>
        @endif

        <div class="cui-layout-footer">
            @include('backend.components.footer')
        </div>
    </div>
</div>

<!-- Scripts -->
@stack('before-scripts')
{!! script(mix('js/manifest.js')) !!}
{!! script(mix('js/vendor.js')) !!}
{!! script(mix('js/vue.js')) !!}
{!! script(mix('js/backend.js')) !!}
@stack('after-scripts')

</body>
</html>
