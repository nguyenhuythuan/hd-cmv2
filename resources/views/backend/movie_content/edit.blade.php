@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.movie_content.show.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')
    <div class="">
        <div id="app">
            <movie-content-edit
                :title="{{ $title }}"
                :actor="{{ $actor }}"
                :director="{{ $director }}"
                :ribbon="{{ $ribbon }}"
                :photo="{{ $photo }}"
                :related="{{ $related }}"
                :dam-info="{{ $damInfo }}"
            />
        </div>
    </div>
@endsection
