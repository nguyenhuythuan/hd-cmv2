@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.movie_content.index.title'))

@section('content')
    @stack('before-scripts')
{{--    {!! script(mix('js/vue.js')) !!}--}}
    @stack('after-scripts')

    <movie-content-list
        :active-movie="{{ $activeTitle }}"
        :un-Active-movie="{{ $unActiveTitle }}"
        :in-Review-movie="{{ $inReviewTitle }}"
    />
@endsection
