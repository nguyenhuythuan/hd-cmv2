@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.page.addNewMobile.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <page-detail-add-new-mobile />

@endsection
