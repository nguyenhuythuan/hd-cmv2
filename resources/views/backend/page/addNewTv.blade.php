@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.page.addNew.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <page-detail-add-new />

@endsection
