@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.page.edit.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <page-detail-edit
            :page="{{ $page }}"
            :ribbon="{{ $ribbon }}"
    />

@endsection
