@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.page.index.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <page-list />
@endsection
