@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.people.edit.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <people-detail-edit
            :people="{{ $people }}"
    />

@endsection
