@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.ribbon.create.title'))

@section('content')
    <ribbon-create />
@endsection