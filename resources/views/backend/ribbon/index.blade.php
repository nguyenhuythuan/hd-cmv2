@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.ribbon.index.title'))

@section('content')
    <ribbon-list />
@endsection
