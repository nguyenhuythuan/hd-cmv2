@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.season.edit.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <season-detail 
        :title="{{ $title }}" 
        :actor="{{ $actor }}" 
        :director="{{ $director }}" 
        :season="{{ $season }}" 
        :episode="{{ $episode }}"
    />

@endsection
