@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.season.index.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')

    <season-list />
@endsection
