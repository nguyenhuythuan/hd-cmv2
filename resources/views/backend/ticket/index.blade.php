@extends('backend.layouts.app')

@section('title', app_name() . ' | ' . __('strings.backend.crm.ticket.title'))

@section('content')
    @stack('before-scripts')
    {!! script(mix('js/vue.js')) !!}
    @stack('after-scripts')
    <div class="">
        <div class="">
            <div id="app">
                <ticket-list />
            </div>
        </div>
    </div>
@endsection
