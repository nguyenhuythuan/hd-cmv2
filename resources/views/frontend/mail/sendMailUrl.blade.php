<p>@lang('strings.emails.contact.email_body_title')</p>
<p><strong>@lang('validation.attributes.frontend.name'):</strong> {{ $request->name }}</p>
<p><strong>@lang('validation.attributes.frontend.email'):</strong> {{ $request->toEmail }}</p>
<p><strong>@lang('validation.attributes.frontend.link'):</strong> {{ $request->link }}</p>
<p><strong>@lang('validation.attributes.frontend.message'):</strong> {{ $request->message }}</p>
