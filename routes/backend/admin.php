<?php

use App\Http\Controllers\Backend\DashboardController;
use App\Http\Controllers\Backend\DeviceController;
use App\Http\Controllers\Backend\ModuleController;
use App\Http\Controllers\Backend\MovieContentController;
use App\Http\Controllers\Backend\TicketController;
use App\Http\Controllers\Backend\PeopleController;
use App\Http\Controllers\Backend\RibbonController;
use App\Http\Controllers\Backend\PageController;

// Module
Route::redirect('/', '/admin/dashboard', 301);
Route::get('dashboard', [DashboardController::class, 'index'])->name('dashboard');
Route::get('module', [ModuleController::class, 'index'])->name('module')->middleware('permission:view module');
Route::get('module/search', [ModuleController::class, 'search'])->name('module.search')->middleware('permission:view module');
Route::get('module/create', [ModuleController::class, 'create'])->name('module.create')->middleware('permission:view module');
Route::get('device', [DeviceController::class, 'index'])->name('device')->middleware('permission:view device');
Route::get('device/getlist', [DeviceController::class, 'getList'])->name('deviceList')->middleware('permission:view device');
Route::get('device/getdetail', [DeviceController::class, 'getDetail'])->name('deviceDetail')->middleware('permission:view device');

// Movie content
Route::get('movie', [MovieContentController::class, 'index'])->name('movie')->middleware('permission:view movie');
Route::get('movie/search', [MovieContentController::class, 'ajaxSearch'])->name('movie.search');
Route::get('movie/suggest', [MovieContentController::class, 'ajaxSuggest'])->name('movie.suggest');
Route::get('movie/searchRibbon', [MovieContentController::class, 'ajaxSearchRibbon'])->name('movie.searchRibbon');
Route::get('movie/searchActor', [MovieContentController::class, 'ajaxSearchActor'])->name('movie.searchActor');
Route::get('movie/searchDirector', [MovieContentController::class, 'ajaxSearchDirector'])->name('movie.searchDirector');
// Route::get('movie/{id}', [MovieContentController::class, 'show'])->name('movie.show')->middleware('permission:view movie');
Route::get('movie/edit/{id}', [MovieContentController::class, 'edit'])->name('movie.edit');
Route::post('movie/update', [MovieContentController::class, 'update'])->name('movie.update');
Route::post('movie/uploadImage', [MovieContentController::class, 'ajaxUploadImage'])->name('movie.uploadImage');
Route::get('movie/getById', [MovieContentController::class, 'getById'])->name('movie.getById');
Route::get('movie/getListDam', [MovieContentController::class, 'ajaxGetListDam'])->name('movie.getListDam');
Route::get('movie/findDam', [MovieContentController::class, 'ajaxFindDam'])->name('movie.findDam');
Route::post('movie/changeStatus', [MovieContentController::class, 'ajaxChangeStatus'])->name('movie.changeStatus');

// Ticket
Route::get('ticket', [TicketController::class, 'index'])->name('ticket');
// people
Route::get('people', [PeopleController::class, 'index'])->name('people');
Route::get('people/searchPeople', [PeopleController::class, 'searchPeople'])->name('people.searchPeople');
Route::get('people/searchIdPeople', [PeopleController::class, 'searchIdPeople'])->name('people.searchIdPeople');
Route::get('people/getPeopleAll', [PeopleController::class, 'getPeopleAll'])->name('people.getPeopleAll');
Route::get('people/addNew', [PeopleController::class, 'addNew'])->name('people.addNew');
Route::post('people/addNewPeople', [PeopleController::class, 'addNewPeople'])->name('people.addNewPeople');
Route::post('people/uploadImage', [PeopleController::class, 'uploadImage'])->name('people.uploadImage');
Route::get('people/edit/{id}', [PeopleController::class, 'edit'])->name('people.edit');
Route::post('people/updatePeople', [PeopleController::class, 'updatePeople'])->name('people.updatePeople');
Route::post('people/import', [PeopleController::class, 'import'])->name('people.import');

//Ribbon
Route::get('ribbon', [RibbonController::class, 'index'])->name('ribbon');
Route::get('ribbon/searchRibbon', [RibbonController::class, 'searchRibbon'])->name('ribbon.searchRibbon');
Route::get('ribbon/searchIdRibbon', [RibbonController::class, 'searchIdRibbon'])->name('ribbon.searchIdRibbon');
Route::get('ribbon/create', [RibbonController::class, 'create'])->name('ribbon.create');
Route::post('ribbon/getTitleByConditions', [RibbonController::class, 'ajaxGetTitleByConditions'])->name('ribbon.getTitileByConditions');
Route::post('ribbon/checkRibbonName', [RibbonController::class, 'ajaxCheckRibbonColumnExist'])->name('ribbon.checkRibbonName');
Route::post('ribbon/store', [RibbonController::class, 'store'])->name('ribbon.store');

//Page
Route::get('page', [PageController::class, 'index'])->name('page');
Route::get('page/addNewTv', [PageController::class, 'addNewTv'])->name('page.addNewTv');
Route::get('page/addNewWebsite', [PageController::class, 'addNewWebsite'])->name('page.addNewWebsite');
Route::get('page/addNewMobile', [PageController::class, 'addNewMobile'])->name('page.addNewMobile');
Route::get('page/searchRibbon', [PageController::class, 'searchRibbon'])->name('page.searchRibbon');
Route::get('page/findIdRibbon', [PageController::class, 'findIdRibbon'])->name('page.findIdRibbon');
Route::post('page/addPage', [PageController::class, 'addPage'])->name('page.addPage');
Route::get('page/searchPage', [PageController::class, 'searchPage'])->name('page.searchPage');
Route::get('page/searchIdPage', [PageController::class, 'searchIdPage'])->name('page.searchIdPage');
Route::get('page/edit/{id}', [PageController::class, 'edit'])->name('page.edit');
Route::get('page/editWebsite/{id}', [PageController::class, 'editWebsite'])->name('page.editWebsite');
Route::get('page/editMobile/{id}', [PageController::class, 'editMobile'])->name('page.editMobile');
Route::post('page/checkPageName', [PageController::class, 'ajaxCheckPageName'])->name('ribbon.page');
Route::post('page/updatePage', [PageController::class, 'updatePage'])->name('page.updatePage');
