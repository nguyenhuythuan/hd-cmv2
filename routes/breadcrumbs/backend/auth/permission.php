<?php

Breadcrumbs::for('admin.auth.permission.index', function ($trail) {
    $trail->parent('admin.dashboard');
    $trail->push(__('menus.backend.access.permissions.management'), route('admin.auth.permission.index'));
});

Breadcrumbs::for('admin.auth.permission.create', function ($trail) {
    $trail->parent('admin.auth.permission.index');
    $trail->push(__('menus.backend.access.permission.create'), route('admin.auth.permission.create'));
});
