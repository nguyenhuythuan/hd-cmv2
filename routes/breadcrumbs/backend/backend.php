<?php

Breadcrumbs::for('admin.dashboard', function ($trail) {
    $trail->push(__('strings.backend.dashboard.title'), route('admin.dashboard'));
});

Breadcrumbs::for('admin.device', function ($trail) {
    $trail->push(__('strings.backend.device.title'), route('admin.device'));
});

Breadcrumbs::for('admin.module', function ($trail) {
    $trail->push(__('strings.backend.module.title'), route('admin.module'));
});

Breadcrumbs::for('admin.module.create', function ($trail) {
    $trail->push(__('strings.backend.module.title'), route('admin.module.create'));
});

// Movie Content
Breadcrumbs::for('admin.movie', function ($trail) {
    $trail->push(__('strings.backend.movie_content.index.title'), route('admin.movie'));
});

Breadcrumbs::for('admin.movie.show', function ($trail, $id) {
    $trail->push(__('strings.backend.movie_content.show.title'), route('admin.movie.show', $id));
});

Breadcrumbs::for('admin.movie.edit', function ($trail, $id) {
    $trail->push(__('strings.backend.movie_content.edit.title'), route('admin.movie.edit', $id));
});

// People
Breadcrumbs::for('admin.people', function ($trail) {
    $trail->push(__('strings.backend.people.index.title'), route('admin.people'));
});

Breadcrumbs::for('admin.people.addNew', function ($trail) {
    $trail->push(__('strings.backend.people.addNew.title'), route('admin.people.addNew'));
});

Breadcrumbs::for('admin.people.edit', function ($trail, $id) {
    $trail->push(__('strings.backend.people.edit.title'), route('admin.people.edit', $id));
});

//Ribbon
Breadcrumbs::for('admin.ribbon', function ($trail) {
    $trail->push(__('strings.backend.ribbon.index.title'), route('admin.ribbon'));
});
Breadcrumbs::for('admin.ribbon.create', function ($trail) {
    $trail->push(__('strings.backend.ribbon.create.title'), route('admin.ribbon.create'));
});

//Page
Breadcrumbs::for('admin.page', function ($trail) {
    $trail->push(__('strings.backend.page.index.title'), route('admin.page'));
});

Breadcrumbs::for('admin.page.addNewTv', function ($trail) {
    $trail->parent('admin.page');
    $trail->push(__('strings.backend.page.addNew.title'), route('admin.page.addNewTv'));
});

Breadcrumbs::for('admin.page.addNewWebsite', function ($trail) {
    $trail->parent('admin.page');
    $trail->push(__('strings.backend.page.addNewWebsite.title'), route('admin.page.addNewWebsite'));
});

Breadcrumbs::for('admin.page.addNewMobile', function ($trail) {
    $trail->parent('admin.page');
    $trail->push(__('strings.backend.page.addNewMobile.title'), route('admin.page.addNewMobile'));
});

Breadcrumbs::for('admin.page.edit', function ($trail, $id) {
    $trail->parent('admin.page');
    $trail->push(__('strings.backend.page.edit.title'), route('admin.page.edit', $id));
});

Breadcrumbs::for('admin.page.editWebsite', function ($trail, $id) {
    $trail->parent('admin.page');
    $trail->push(__('strings.backend.page.edit.title'), route('admin.page.editWebsite', $id));
});

Breadcrumbs::for('admin.page.editMobile', function ($trail, $id) {
    $trail->parent('admin.page');
    $trail->push(__('strings.backend.page.edit.title'), route('admin.page.editMobile', $id));
});

require __DIR__.'/auth.php';
require __DIR__.'/log-viewer.php';
